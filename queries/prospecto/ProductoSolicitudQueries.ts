import ProductoSolicitudModel from "../../models/venta-nueva/prospecto/productoSolicitudModel";
import {Op, QueryTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";

export default class ProductoSolicitudQueries {
    static async getByIdProspecto(idProspecto) {
        return await ProductoSolicitudModel.findAll({
            where: {idProspecto, datos:{[Op.not]: null}}
        })
    }

    static async getById(id) {
        return await dbPostgres.query(`
        SELECT
        operaciones."productoSolicitud"."id",
        operaciones."productoSolicitud".datos,
        operaciones.prospecto.numero,
        operaciones.prospecto.correo,
        operaciones.prospecto.nombre,
        operaciones.prospecto.sexo,
        operaciones.prospecto.edad
        FROM
        operaciones."productoSolicitud"
        INNER JOIN
        operaciones.prospecto
        ON
        operaciones."productoSolicitud"."idProspecto" = operaciones.prospecto."id"
        WHERE "productoSolicitud".id = ${id}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }

    static async post(data) {
        return await ProductoSolicitudModel.create(data).then(data => {
            return data.id;
        })
    }
}

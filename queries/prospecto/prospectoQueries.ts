import ProspectoModel from "../../models/venta-nueva/prospecto/prospectoModel";

export default class ProspectoQueries {
    static async getByCorreo(correo) {
        return await ProspectoModel.findOne({
            raw: true,
            where: {correo}
        })
    }

    static async post(data) {
        return await ProspectoModel.create(data).then(data => {
            return data['id'];
        })
    }

    static async update(data, id) {
        return await ProspectoModel.update(data, {
            where: {id},
        })
    }
}

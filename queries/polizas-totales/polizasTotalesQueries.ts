import {QueryTypes} from 'sequelize';
import {dbPostgres} from "../../configs/connection";

export default class Polizas {
    static async findAllPolizas(fechaInicio,fechaFin): Promise<any> {
        return await dbPostgres.query(`
        SELECT
        operaciones.registro."id",
        operaciones.registro."idEmpleado",
        operaciones.registro."idProducto",
        operaciones.registro."idTipoPago",
        operaciones.registro."idProductoSocio",
        operaciones.registro."idFlujoPoliza",
        operaciones.registro.poliza,
        operaciones.registro."fechaInicio",
        operaciones.registro."primaNeta",
        operaciones.registro."fechaRegistro",
        operaciones.registro.archivo,
        operaciones."tipoPago"."tipoPago" "tipoPago",
        operaciones."productosSocio".nombre "productoSocio",
        operaciones."productoCliente".datos "datos",
        operaciones.registro."idEstadoPoliza" "idEstadoPoliza",
        operaciones."estadoPoliza".estado AS "estadoPoliza",
        "recursosHumanos".departamento."idArea" "idArea",
        "recursosHumanos".precandidato.nombre,
        "recursosHumanos".precandidato."apellidoPaterno" "apellidoPaterno",
        "recursosHumanos".precandidato."apellidoMaterno" "apellidoMaterno",
        "recursosHumanos".plaza."idPuesto",
        "recursosHumanos".plaza."idDepartamento" "idSubarea",
        operaciones."productoCliente".datos->> 'descripcion' "descripcion1",
        CASE WHEN operaciones."productoCliente"."noSerie" IS NOT NULL THEN operaciones."productoCliente"."noSerie" WHEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' IS NOT NULL THEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
        "recursosHumanos".departamento.descripcion "descripcion",
        operaciones.cliente."idPais",
        operaciones."productoCliente"."idCliente",
        operaciones."productoCliente"."idSolicitud",
        operaciones."productoCliente"."idSubRamo",
        operaciones.cliente.nombre "nombreCliente",
        operaciones.cliente.paterno "apellidoPaternoCliente",
        operaciones.cliente.materno "apellidoMaternoCliente",
        operaciones.cliente.correo,
        operaciones.cliente."telefonoMovil",
        CASE WHEN operaciones."productoCliente".archivo IS NOT NULL THEN operaciones."productoCliente".archivo ELSE operaciones.cliente.archivo END as "carpetaCliente",
        operaciones.recibos."id" "idRecibo",
        operaciones.recibos."idEstadoRecibos" "idEstadoRecibo",
        operaciones.recibos."fechaLiquidacion",
        operaciones."estadoRecibo"."estadoDescripcion" "estadoRecibo",
        operaciones.recibos."fechaPromesaPago",
        operaciones.cliente."archivoSubido",
        operaciones.cliente."genero",
        operaciones.cliente."cp",
        operaciones.cliente."fechaNacimiento",
        operaciones.socios."nombreComercial",
        operaciones."estadoPago".descripcion AS "estadoPago",
        operaciones.registro."idSocio"
        FROM
        operaciones.registro
        INNER JOIN operaciones."tipoPago" on operaciones."tipoPago"."id" = operaciones.registro."idTipoPago"
        INNER JOIN operaciones."productoCliente" on operaciones."productoCliente"."id" = operaciones.registro."idProducto"
        INNER JOIN operaciones.cliente on operaciones.cliente."id" = operaciones."productoCliente"."idCliente"
        INNER JOIN "recursosHumanos".empleado on "recursosHumanos".empleado."id" = operaciones.registro."idEmpleado"
        INNER JOIN "recursosHumanos".candidato on "recursosHumanos".candidato."id" = "recursosHumanos".empleado."idCandidato"
        INNER JOIN "recursosHumanos".precandidato on "recursosHumanos".precandidato."id" = "recursosHumanos".candidato."idPrecandidato"
        INNER JOIN operaciones.socios on operaciones.socios."id" = operaciones.registro."idSocio"
        INNER JOIN operaciones."estadoPoliza" on operaciones."estadoPoliza"."id" = operaciones.registro."idEstadoPoliza"
        INNER JOIN operaciones.recibos on operaciones.recibos."idRegistro" = operaciones.registro."id"
        INNER JOIN operaciones."estadoRecibo" on operaciones."estadoRecibo"."id" = operaciones.recibos."idEstadoRecibos"
        INNER JOIN operaciones."productosSocio" on operaciones."productosSocio"."id" = operaciones.registro."idProductoSocio"
        LEFT JOIN operaciones.usuarios on operaciones.usuarios."idEmpleado" = "recursosHumanos".empleado."id"
        LEFT JOIN "recursosHumanos".plaza on "recursosHumanos".plaza."id" = "recursosHumanos".empleado."idPlaza"
        LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento."id" = "recursosHumanos".plaza."idDepartamento"
        LEFT JOIN "recursosHumanos".area on "recursosHumanos".area."id" = "recursosHumanos".departamento."idArea"
        LEFT JOIN operaciones.pagos ON operaciones.recibos.id = operaciones.pagos."idRecibo"
        LEFT JOIN operaciones."estadoPago" ON operaciones.pagos."idEstadoPago" = operaciones."estadoPago".id
        WHERE operaciones.registro."fechaInicio" BETWEEN  ('${fechaInicio}') AND ('${fechaFin}')
        AND operaciones.recibos.numero = 1
        AND operaciones.registro."idEstadoPoliza" <> 9
        `, {
            type: QueryTypes.SELECT
        });
    }
    static async findAllCanceladas(fechaInicio,fechaFin): Promise<any> {
        return await dbPostgres.query(`
        SELECT
        operaciones.registro."id",
        operaciones.registro."idEmpleado",
        operaciones.registro."idProducto",
        operaciones.registro."idTipoPago",
        operaciones.registro."idProductoSocio",
        operaciones.registro."idFlujoPoliza",
        operaciones.registro.poliza,
        operaciones.registro."fechaInicio",
        operaciones.registro."primaNeta",
        operaciones.registro."fechaRegistro",
        operaciones.registro.archivo,
        operaciones."tipoPago"."tipoPago" "tipoPago",
        operaciones."productosSocio".nombre "productoSocio",
        operaciones."productoCliente".datos "datos",
        operaciones.registro."idEstadoPoliza" "idEstadoPoliza",
        operaciones."estadoPoliza".estado as "estadoPoliza",
        "recursosHumanos".departamento."idArea" "idArea",
        "recursosHumanos".precandidato.nombre,
        "recursosHumanos".precandidato."apellidoPaterno" "apellidoPaterno",
        "recursosHumanos".precandidato."apellidoMaterno" "apellidoMaterno",
        "recursosHumanos".plaza."idPuesto",
        "recursosHumanos".plaza."idDepartamento" "idSubarea",
        operaciones."productoCliente".datos->> 'descripcion' "descripcion",
        CASE WHEN operaciones."productoCliente"."noSerie" IS NOT NULL THEN operaciones."productoCliente"."noSerie" WHEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' IS NOT NULL THEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
        "recursosHumanos".departamento.descripcion "subarea",
        operaciones.cliente."idPais",
        operaciones."productoCliente"."idCliente",
        operaciones."productoCliente"."idSolicitud",
        operaciones."productoCliente"."idSubRamo",
        operaciones.cliente.nombre "nombreCliente",
        operaciones.cliente.paterno "apellidoPaternoCliente",
        operaciones.cliente.materno "apellidoMaternoCliente",
        operaciones.cliente.correo,
        operaciones.cliente."telefonoMovil",
        CASE WHEN operaciones."productoCliente".archivo IS NOT NULL THEN operaciones."productoCliente".archivo ELSE operaciones.cliente.archivo END as "carpetaCliente",
        operaciones.recibos."id" "idRecibo",
        operaciones.recibos."idEstadoRecibos" "idEstadoRecibo",
        operaciones.recibos."fechaLiquidacion",
        operaciones."estadoRecibo"."estadoDescripcion" "estadoRecibo",
        operaciones.recibos."fechaPromesaPago",
        operaciones.cliente."archivoSubido",
        operaciones."estadoPago".descripcion AS "estadoPago",
        operaciones.cliente."genero",
        operaciones.cliente."cp",
        operaciones.cliente."fechaNacimiento",
        operaciones.socios."nombreComercial",
        operaciones.registro."idSocio"
        FROM
        operaciones.registro
        INNER JOIN operaciones."tipoPago" on operaciones."tipoPago"."id" = operaciones.registro."idTipoPago"
        INNER JOIN operaciones."productoCliente" on operaciones."productoCliente"."id" = operaciones.registro."idProducto"
        INNER JOIN operaciones.cliente on operaciones.cliente."id" = operaciones."productoCliente"."idCliente"
        INNER JOIN "recursosHumanos".empleado on "recursosHumanos".empleado."id" = operaciones.registro."idEmpleado"
        INNER JOIN "recursosHumanos".candidato on "recursosHumanos".candidato."id" = "recursosHumanos".empleado."idCandidato"
        INNER JOIN "recursosHumanos".precandidato on "recursosHumanos".precandidato."id" = "recursosHumanos".candidato."idPrecandidato"
        INNER JOIN operaciones.socios on operaciones.socios."id" = operaciones.registro."idSocio"
        INNER JOIN operaciones."estadoPoliza" on operaciones."estadoPoliza"."id" = operaciones.registro."idEstadoPoliza"
        INNER JOIN operaciones.recibos on operaciones.recibos."idRegistro" = operaciones.registro."id"
        INNER JOIN operaciones."estadoRecibo" on operaciones."estadoRecibo"."id" = operaciones.recibos."idEstadoRecibos"
        INNER JOIN operaciones."productosSocio" on operaciones."productosSocio"."id" = operaciones.registro."idProductoSocio"
        INNER JOIN operaciones.usuarios on operaciones.usuarios."idEmpleado" = "recursosHumanos".empleado."id"
        INNER JOIN "recursosHumanos".plaza on "recursosHumanos".plaza."id" = "recursosHumanos".empleado."idPlaza"
        INNER JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento."id" = "recursosHumanos".plaza."idDepartamento"
        INNER JOIN "recursosHumanos".area on "recursosHumanos".area."id" = "recursosHumanos".departamento."idArea"
        LEFT JOIN operaciones.pagos ON operaciones.recibos.id = operaciones.pagos."idRecibo"
        LEFT JOIN operaciones."estadoPago" ON operaciones.pagos."idEstadoPago" = operaciones."estadoPago".id
        WHERE operaciones.registro."fechaInicio" BETWEEN  ('${fechaInicio}') AND ('${fechaFin}') AND operaciones.registro."idEstadoPoliza" = 9
        AND operaciones.recibos.numero = 1
        `, {
            type: QueryTypes.SELECT
        });
    }
    static async findId(id): Promise<any> {
        return await dbPostgres.query(`
        SELECT
        operaciones.registro."id",
        operaciones.registro."idEmpleado",
        operaciones.registro."idProducto",
        operaciones.registro."idTipoPago",
        operaciones.registro."idProductoSocio",
        operaciones.registro."idFlujoPoliza",
        operaciones.registro.poliza,
        operaciones.registro."fechaInicio",
        operaciones.registro."primaNeta",
        operaciones.registro."fechaRegistro",
        operaciones.registro.archivo,
        operaciones."tipoPago"."tipoPago" "tipoPago",
        operaciones."productosSocio".nombre "productoSocio",
        operaciones."productoCliente".datos "datos",
        operaciones.registro."idEstadoPoliza" "idEstadoPoliza",
        operaciones."estadoPoliza".estado,
        "recursosHumanos".departamento."idArea" "idArea",
        "recursosHumanos".precandidato.nombre,
        "recursosHumanos".precandidato."apellidoPaterno" "apellidoPaterno",
        "recursosHumanos".precandidato."apellidoMaterno" "apellidoMaterno",
        "recursosHumanos".plaza."idPuesto",
        "recursosHumanos".plaza."idDepartamento" "idSubarea",
        operaciones."productoCliente".datos->> 'descripcion' "descripcion",
        CASE WHEN operaciones."productoCliente"."noSerie" IS NOT NULL THEN operaciones."productoCliente"."noSerie" WHEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' IS NOT NULL THEN operaciones."productoCliente".datos :: json ->> 'numeroSerie' ELSE  'SIN NUMERO DE SERIE' END AS "numeroSerie",
        "recursosHumanos".departamento.descripcion "subarea",
        operaciones.cliente."idPais",
        operaciones."productoCliente"."idCliente",
        operaciones."productoCliente"."idSolicitud",
        operaciones."productoCliente"."idSubRamo",
        operaciones.cliente.nombre "nombreCliente",
        operaciones.cliente.paterno "apellidoPaternoCliente",
        operaciones.cliente.materno "apellidoMaternoCliente",
        operaciones.cliente.correo,
        operaciones.cliente."telefonoMovil",
        CASE WHEN operaciones."productoCliente".archivo IS NOT NULL THEN operaciones."productoCliente".archivo ELSE operaciones.cliente.archivo END as "carpetaCliente",
        operaciones.recibos."id" "idRecibo",
        operaciones.recibos."idEstadoRecibos" "idEstadoRecibo",
        operaciones.recibos."fechaLiquidacion",
        operaciones."estadoRecibo"."estadoDescripcion" "estadoRecibo",
        operaciones.recibos."fechaPromesaPago",
        operaciones.cliente."archivoSubido",
        operaciones.cliente."genero",
        operaciones.cliente."cp",
        operaciones.cliente."fechaNacimiento",
        operaciones.socios."nombreComercial",
        operaciones.registro."idSocio"
        FROM
        operaciones.registro
        INNER JOIN operaciones."tipoPago" on operaciones."tipoPago"."id" = operaciones.registro."idTipoPago"
        INNER JOIN operaciones."productoCliente" on operaciones."productoCliente"."id" = operaciones.registro."idProducto"
        INNER JOIN operaciones.cliente on operaciones.cliente."id" = operaciones."productoCliente"."idCliente"
        INNER JOIN "recursosHumanos".empleado on "recursosHumanos".empleado."id" = operaciones.registro."idEmpleado"
        INNER JOIN "recursosHumanos".candidato on "recursosHumanos".candidato."id" = "recursosHumanos".empleado."idCandidato"
        INNER JOIN "recursosHumanos".precandidato on "recursosHumanos".precandidato."id" = "recursosHumanos".candidato."idPrecandidato"
        INNER JOIN operaciones.socios on operaciones.socios."id" = operaciones.registro."idSocio"
        INNER JOIN operaciones."estadoPoliza" on operaciones."estadoPoliza"."id" = operaciones.registro."idEstadoPoliza"
        INNER JOIN operaciones.recibos on operaciones.recibos."idRegistro" = operaciones.registro."id"
        INNER JOIN operaciones."estadoRecibo" on operaciones."estadoRecibo"."id" = operaciones.recibos."idEstadoRecibos"
        INNER JOIN operaciones."productosSocio" on operaciones."productosSocio"."id" = operaciones.registro."idProductoSocio"
        LEFT JOIN operaciones.usuarios on operaciones.usuarios."idEmpleado" = "recursosHumanos".empleado."id"
        LEFT JOIN "recursosHumanos".plaza on "recursosHumanos".plaza."id" = "recursosHumanos".empleado."idPlaza"
        LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".departamento."id" = "recursosHumanos".plaza."idDepartamento"
        LEFT JOIN "recursosHumanos".area on "recursosHumanos".area."id" = "recursosHumanos".departamento."idArea"
        WHERE operaciones.registro.id = ${id} AND operaciones.recibos.numero = 1
        `, {
            type: QueryTypes.SELECT
        });
    }
}

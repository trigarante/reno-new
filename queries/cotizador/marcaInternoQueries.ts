import MarcaInternoModel from "../../models/venta-nueva/cotizador/marca/marcaInternoModel";

export default class MarcaInternoQueries {
    static async getByIdTipoSubRamo(idTipoSubRamo) {
        return await MarcaInternoModel.findAll({
            where: {idTipoSubRamo}
        })
    }
}

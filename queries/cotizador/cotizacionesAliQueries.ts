import CotizacionesAliModel from "../../models/venta-nueva/cotizador/cotizacionesAliModel";

export default class CotizacionesAliQueries {
    static async post(data) {
        return await CotizacionesAliModel.create(data).then(data => {
           return data['id'];
        });
    }
    static async getById(id) {
        return await CotizacionesAliModel.findAll({
            where: {
                id
            },
        })
    }
}

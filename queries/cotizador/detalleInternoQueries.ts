import DetalleInternoModel from "../../models/venta-nueva/cotizador/detalleInterno/detalleInternoModel";

export default class DetalleInternoQueries {
    static async getByIdMarcaAndIdModelo(idMarca, idModelo) {
        return await DetalleInternoModel.findAll({
            where: {idMarca, idModelo},
            order: [
                ['descripcion', 'ASC']
            ],
        })
    }

    static async getByIdMarcaAndIdModeloAndIdDescripcion(idMarca, idModelo, idDescripcion) {
        return await DetalleInternoModel.findAll({
            where: {idMarca, idModelo, idDescripcion},
            order: [
                ['subDescripcion', 'ASC'],
            ]
        })
    }

    static async getByIdMarcaAndIdModeloAndIdDescripcionAndIdSubDescripcion(idMarca, idModelo, idDescripcion, idSubDescripcion) {
        return await DetalleInternoModel.findAll({
            where: {idMarca, idModelo, idDescripcion, idSubDescripcion},
            order: [
                ['detalle', 'ASC'],
            ]
        })
    }
}

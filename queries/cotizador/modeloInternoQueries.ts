import ModeloInternoModel from "../../models/venta-nueva/cotizador/modeloInterno/modeloInternoModel";

export default class ModeloInternoQueries {
    static async getAll() {
        return await ModeloInternoModel.findAll({
            order: [
                ['nombre', 'DESC']
            ]
        })
    }
}

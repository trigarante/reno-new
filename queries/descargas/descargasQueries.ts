import {QueryTypes} from 'sequelize';
import {dbPostgres} from "../../configs/connection";

export default class DescargasQueries {
    static async pnc(fechaInicio,fechaFin): Promise<any> {
        return await dbPostgres.query(`
SELECT
DISTINCT ON (operaciones.registro.poliza)
operaciones.registro.poliza AS "numeroPoliza",
operaciones.registro."fechaInicio" AS "fechaInicio",
operaciones."estadoPoliza".estado AS "estadoPoliza",
operaciones."tipoPago"."tipoPago" AS "tipoPago",
operaciones."formaPago".descripcion AS "metodoPago",
operaciones.socios."nombreComercial" AS "aseguradora",
operaciones."productosSocio".nombre AS "cobertura",
operaciones."tipoSubRamo".tipo AS "tipoUso",
CASE
WHEN operaciones."productoCliente"."noSerie" IS  NULL
THEN operaciones."productoCliente".datos ->> 'numeroSerie'
ELSE operaciones."productoCliente"."noSerie"
END AS "noSerie",
concat_ws(' ',precandidato.nombre, ' ' , precandidato."apellidoPaterno", ' ' , precandidato."apellidoMaterno") AS "nombreEjecutivo",
"recursosHumanos".precandidato."id" AS "idEmpleado",
"recursosHumanos".area.nombre AS "areaEjecutivo",
"recursosHumanos".area."id" AS "idArea",
"recursosHumanos".departamento.descripcion AS "departamento",
"recursosHumanos".departamento."id" AS "idDepartamento",
operaciones.recibos.cantidad AS "montoPago",
operaciones.pagos."fechaPago" AS "fechaPago",
operaciones.recibos."fechaLiquidacion" AS "fechaAplicacion"

FROM  operaciones.registro

INNER JOIN operaciones."productoCliente" ON (operaciones.registro."idProducto" = operaciones."productoCliente"."id")
INNER JOIN operaciones.cliente ON (operaciones."productoCliente"."idCliente" = operaciones.cliente."id")
LEFT JOIN operaciones.recibos ON (operaciones.registro."id" = operaciones.recibos."idRegistro")
LEFT JOIN operaciones.pagos ON (operaciones.recibos."id" = operaciones.pagos."idRecibo")
LEFT JOIN operaciones."tipoPago" ON (operaciones.registro."idTipoPago" = operaciones."tipoPago"."id")
LEFT JOIN operaciones."formaPago" ON (operaciones.pagos."idFormaPago" = operaciones."formaPago"."id")
INNER JOIN "recursosHumanos".empleado ON (operaciones.recibos."idEmpleado" = "recursosHumanos".empleado."id")
INNER JOIN "recursosHumanos".candidato ON (empleado."idCandidato"= candidato."id")
INNER JOIN "recursosHumanos".precandidato on (candidato."idPrecandidato"=precandidato."id")
INNER JOIN "recursosHumanos"."plaza" on ("empleado"."idPlaza"=plaza."id")
INNER JOIN "recursosHumanos".departamento on (plaza."idDepartamento"=departamento."id")
INNER JOIN "recursosHumanos".area ON ("recursosHumanos".departamento."idArea"= "recursosHumanos".area."id")
INNER JOIN operaciones."estadoPoliza" ON (operaciones.registro."idEstadoPoliza" = operaciones."estadoPoliza"."id")
INNER JOIN operaciones."estadoRecibo" ON (operaciones.recibos."idEstadoRecibos" = operaciones."estadoRecibo"."id")
LEFT JOIN operaciones."productosSocio" ON (operaciones.registro."idProductoSocio" = operaciones."productosSocio"."id")
LEFT JOIN operaciones."subRamo" ON (operaciones."productosSocio"."idSubRamo" = operaciones."subRamo"."id")
LEFT JOIN operaciones."tipoSubRamo" ON (operaciones."subRamo"."idTipoSubRamo"= operaciones."tipoSubRamo"."id")
LEFT JOIN operaciones.ramo on (operaciones."subRamo"."idRamo" = operaciones.ramo."id")
INNER JOIN "recursosHumanos".sede ON ("recursosHumanos".plaza."idSede"= "recursosHumanos".sede."id")
LEFT JOIN operaciones.socios ON (operaciones.ramo."idSocio" = operaciones.socios."id")
LEFT JOIN operaciones.bancos on ((operaciones.pagos.datos ->> 'idBanco') :: BIGINT = "operaciones".bancos."id")
LEFT JOIN "recursosHumanos".departamento dep on (dep."id"=registro."idDepartamento")
LEFT JOIN "recursosHumanos".departamento depa on (depa."id"=dep."id")

WHERE operaciones.pagos."idEstadoPago" = 1
AND operaciones.pagos."fechaPago" BETWEEN '${fechaInicio}' and '${fechaFin}'
AND operaciones.recibos.activo = 1
AND operaciones.recibos.numero = 1
AND operaciones.registro."idFlujoPoliza" = 9
        `, {
            type: QueryTypes.SELECT
        });
    }

    static async pncSUB(fechaInicio,fechaFin): Promise<any> {
        return await dbPostgres.query(`

SELECT
DISTINCT ON (operaciones.registro.poliza)
operaciones.registro.poliza AS "numeroPoliza",
operaciones.registro."fechaInicio" AS "fechaInicio",
operaciones."estadoPoliza".estado AS "estadoPoliza",
operaciones."tipoPago"."tipoPago" AS "tipoPago",
operaciones."formaPago".descripcion AS "metodoPago",
operaciones.socios."nombreComercial" AS "aseguradora",
operaciones."productosSocio".nombre AS "cobertura",
operaciones."tipoSubRamo".tipo AS "tipoUso",
CASE
WHEN operaciones."productoCliente"."noSerie" IS  NULL
THEN operaciones."productoCliente".datos ->> 'numeroSerie'
ELSE operaciones."productoCliente"."noSerie"
END AS "noSerie",
concat_ws(' ',precandidato.nombre, ' ' , precandidato."apellidoPaterno", ' ' , precandidato."apellidoMaterno") AS "nombreEjecutivo",
operaciones.pagos."idEmpleado" AS "idEmpleado",
concat_ws(' ',pr.nombre, ' ' , pr."apellidoPaterno", ' ' , pr."apellidoMaterno") AS "nombreEjecutivoRegistro",
em."id" as "idEmpleadoRegistro",
dep.descripcion as "departamentoPoliza",
"recursosHumanos".area.nombre AS "areaEjecutivo",
"recursosHumanos".area."id" AS "idArea",
"recursosHumanos".departamento.descripcion AS "departamento",
"recursosHumanos".departamento."id" AS "idDepartamento",
operaciones.recibos.cantidad AS "montoPago",
operaciones.pagos."fechaPago" AS "fechaPago",
operaciones.recibos."fechaLiquidacion" AS "fechaAplicacion"

FROM  operaciones.registro

INNER JOIN operaciones."productoCliente" ON (operaciones.registro."idProducto" = operaciones."productoCliente"."id")
INNER JOIN operaciones.cliente ON (operaciones."productoCliente"."idCliente" = operaciones.cliente."id")
LEFT JOIN operaciones.recibos ON (operaciones.registro."id" = operaciones.recibos."idRegistro")
LEFT JOIN operaciones.pagos ON (operaciones.recibos."id" = operaciones.pagos."idRecibo")
LEFT JOIN operaciones."tipoPago" ON (operaciones.registro."idTipoPago" = operaciones."tipoPago"."id")
LEFT JOIN operaciones."formaPago" ON (operaciones.pagos."idFormaPago" = operaciones."formaPago"."id")
INNER JOIN "recursosHumanos".empleado ON (operaciones.pagos."idEmpleado" = "recursosHumanos".empleado."id")
INNER JOIN "recursosHumanos".candidato ON (empleado."idCandidato"= candidato."id")
INNER JOIN "recursosHumanos".precandidato on (candidato."idPrecandidato"=precandidato."id")
INNER JOIN "recursosHumanos"."plaza" on ("empleado"."idPlaza"=plaza."id")
INNER JOIN "recursosHumanos".departamento on (plaza."idDepartamento"=departamento."id")
INNER JOIN "recursosHumanos".area ON ("recursosHumanos".departamento."idArea"= "recursosHumanos".area."id")
INNER JOIN operaciones."estadoPoliza" ON (operaciones.registro."idEstadoPoliza" = operaciones."estadoPoliza"."id")
INNER JOIN operaciones."estadoRecibo" ON (operaciones.recibos."idEstadoRecibos" = operaciones."estadoRecibo"."id")
LEFT JOIN operaciones."productosSocio" ON (operaciones.registro."idProductoSocio" = operaciones."productosSocio"."id")
LEFT JOIN operaciones."subRamo" ON (operaciones."productosSocio"."idSubRamo" = operaciones."subRamo"."id")
LEFT JOIN operaciones."tipoSubRamo" ON (operaciones."subRamo"."idTipoSubRamo"= operaciones."tipoSubRamo"."id")
LEFT JOIN operaciones.ramo on (operaciones."subRamo"."idRamo" = operaciones.ramo."id")
INNER JOIN "recursosHumanos".sede ON ("recursosHumanos".plaza."idSede"= "recursosHumanos".sede."id")
LEFT JOIN operaciones.socios ON (operaciones.ramo."idSocio" = operaciones.socios."id")
LEFT JOIN operaciones.bancos on ((operaciones.pagos.datos ->> 'idBanco') :: BIGINT = "operaciones".bancos."id")
LEFT JOIN "recursosHumanos".departamento dep on (dep."id"=registro."idDepartamento")
LEFT JOIN "recursosHumanos".departamento depa on (depa."id"=dep."id")
LEFT JOIN "recursosHumanos".empleado em on (operaciones.registro."idEmpleado"=em."id")
LEFT JOIN "recursosHumanos".candidato ca on (em."idCandidato"=ca."id")
LEFT JOIN "recursosHumanos".precandidato pr on (ca."idPrecandidato"=pr."id")

WHERE operaciones.pagos."idEstadoPago" = 1
AND operaciones.pagos."fechaPago" BETWEEN '${fechaInicio}' and '${fechaFin}'
AND operaciones.recibos.activo = 1
AND operaciones.recibos.numero <> 1
AND "recursosHumanos".departamento."id" IN (73,177,178,179,270)

        `, {
            type: QueryTypes.SELECT
        });
    }

    static async avanceEjecutivos(fechaInicio,fechaFin): Promise<any> {
        return await dbPostgres.query(`
SELECT
generales.emisiones."idEmpleadoRenovacion" AS "idEmpleado",
CONCAT_WS( ' ',"recursosHumanos".precandidato.nombre, "recursosHumanos".precandidato."apellidoPaterno","recursosHumanos".precandidato."apellidoMaterno") AS ejecutivo,
COUNT( DISTINCT generales.emisiones.poliza ) AS base,
SUM(
CASE WHEN operaciones.registro.id IS NOT NULL AND generales.emisiones."idTipoRenovacion"=1 AND operaciones.registro."idEstadoPoliza" <> 9
 THEN 1 ELSE 0 END ) AS renovadas,
SUM(
CASE WHEN operaciones.registro.id IS NOT NULL AND generales.emisiones."idTipoRenovacion" = 2 AND operaciones.registro."idEstadoPoliza" <> 9
THEN 1 ELSE 0 END) AS reexpedidas,
SUM(
CASE WHEN operaciones.registro.id IS NOT NULL AND generales.emisiones."idTipoRenovacion" = 3 AND operaciones.registro."idEstadoPoliza" <> 9
THEN 1 ELSE 0 END) AS migradas,
SUM(
CASE WHEN operaciones.registro.id IS NOT NULL
THEN 1 ELSE 0  END) AS gestion,
SUM(
CASE WHEN operaciones.registro."idEstadoPoliza"=9
THEN 1 ELSE 0 END) AS canceladas
FROM
generales.emisiones
LEFT JOIN operaciones."productoCliente" ON generales.emisiones.id = operaciones."productoCliente"."idEmision"
LEFT JOIN operaciones.registro ON operaciones."productoCliente".id = operaciones.registro."idProducto"
INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = generales.emisiones."idEmpleadoRenovacion"
INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato"= "recursosHumanos".candidato.id
INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id 
WHERE
generales.emisiones."fechaInicio" BETWEEN '${fechaInicio}'
AND '${fechaFin}'
GROUP BY
1,2
        `, {
            type: QueryTypes.SELECT
        });
    }

    static async llamadas(fechaInicio,fechaFin): Promise<any> {
        return await dbPostgres.query(`
SELECT
DISTINCT ON (generales.emisiones."idEmpleadoRenovacion")

generales.emisiones."idEmpleadoRenovacion" AS "idEmpleado",
CONCAT_WS( ' ', "recursosHumanos".precandidato.nombre,"recursosHumanos".precandidato."apellidoPaterno","recursosHumanos".precandidato."apellidoMaterno") AS ejecutivo,

CASE WHEN "llamadasINEmpleados".llamadas IS NULL
THEN 0 ELSE "llamadasINEmpleados".llamadas END AS acd,

 CASE WHEN "llamadasINEmpleados"."tiempoProduc" / "llamadasINEmpleados".llamadas IS NULL
 THEN 0
 ELSE "llamadasINEmpleados"."tiempoProduc"/ "llamadasINEmpleados".llamadas END AS "tiempoMedioAcd",
 
 CASE WHEN "llamadasEmpleados".llamadas IS NULL
THEN 0 ELSE "llamadasEmpleados".llamadas END AS "out",

CASE WHEN "llamadasEmpleados"."tiempoProduc" / "llamadasEmpleados".llamadas IS NULL
 THEN 0 ELSE  "llamadasEmpleados"."tiempoProduc"/ "llamadasEmpleados".llamadas END AS "tiempoMedioOut",


 CASE WHEN pausas."totalTiempoPausas" IS NULL
 THEN 0 ELSE pausas."totalTiempoPausas" END AS "tiempoPausa",


(CASE WHEN "llamadasINEmpleados"."tiempoProduc" IS NULL
 THEN 0 ELSE "llamadasINEmpleados"."tiempoProduc" END) +

 (CASE WHEN "llamadasEmpleados"."tiempoProduc" IS NULL
 THEN 0 ELSE "llamadasEmpleados"."tiempoProduc" END) AS "tiempoProductivo", 

 
'EN CONSTRUCCION' AS "llamadasAbandonadas",

CASE WHEN logueos."horaLogueo" IS NULL
THEN 0 ELSE logueos."horaLogueo" END AS "horaLogueo",
CASE WHEN logueos."tiempoLogueadoMinMaxSeg" IS NULL
THEN 0 ELSE logueos."tiempoLogueadoMinMaxSeg" END AS "tiempoLogueo"

--  -----------------------------------------------------------------------------------------------------------------------------------
FROM
generales.emisiones
INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = generales.emisiones."idEmpleadoRenovacion"
INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" ="recursosHumanos".candidato.id
INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id

LEFT JOIN (
SELECT
operaciones.llamadas."idEmpleado" AS "idEmpleado",
SUM(
CASE WHEN operaciones.llamadas."fechaInicio" IS NOT NULL
THEN 1 ELSE 0 END) AS llamadas,
SUM(
CASE WHEN operaciones.llamadas."fechaInicio" IS NOT NULL AND operaciones.llamadas."fechaFinal" IS NOT NULL
THEN EXTRACT(EPOCH FROM operaciones.llamadas."fechaFinal" ::TIMESTAMP-operaciones.llamadas."fechaInicio" ::TIMESTAMP)
ELSE 0 END) AS "tiempoProduc"

FROM
operaciones.llamadas 
WHERE
DATE(operaciones.llamadas."fechaInicio") BETWEEN '${fechaInicio}' 
AND '${fechaFin}'
  AND operaciones.llamadas."idTipoLlamada"=4   
GROUP BY
operaciones.llamadas."idEmpleado"

) "llamadasEmpleados" ON "llamadasEmpleados"."idEmpleado" = "recursosHumanos".empleado.id

-----------------------------------------------------------------------------------------------------------------------------------------

LEFT JOIN (
SELECT
llamadas."idEmpleado" AS "idEmpleado",
SUM(
CASE WHEN operaciones.llamadas."fechaInicio" IS NOT NULL
THEN 1 ELSE 0 END) AS llamadas,
SUM(
    CASE WHEN operaciones.llamadas."fechaInicio" IS NOT NULL AND llamadas."fechaFinal" IS NOT NULL
THEN EXTRACT(EPOCH FROM operaciones.llamadas."fechaFinal" ::TIMESTAMP-operaciones.llamadas."fechaInicio" ::TIMESTAMP)
ELSE 0 END) AS "tiempoProduc"
FROM
operaciones.llamadas 
WHERE
DATE(operaciones.llamadas."fechaInicio") BETWEEN '${fechaInicio}' 
AND '${fechaFin}'
AND operaciones.llamadas."idTipoLlamada"=3  
GROUP BY
operaciones.llamadas."idEmpleado"


) "llamadasINEmpleados" ON "llamadasINEmpleados"."idEmpleado" = "recursosHumanos".empleado.id 


-- --------------------------------------------------------------------------------------------------------------------------------
-- LOGIN

LEFT JOIN (

SELECT
"logueosD"."idEmpleado" AS "idEmpleado",
EXTRACT(EPOCH FROM min("logueosD".inicio)::TIME) AS "horaLogueo",
CASE WHEN max("logueosD".fin) IS NULL 
THEN EXTRACT(EPOCH FROM NOW()::TIMESTAMP - "logueosD".inicio::TIMESTAMP)
ELSE EXTRACT(EPOCH FROM "logueosD".fin::TIMESTAMP- "logueosD".inicio::TIMESTAMP)
END AS "tiempoLogueadoMinMaxSeg"

FROM
(
  SELECT  
operaciones.usuarios."idEmpleado" AS "idEmpleado",
generales."sesionUsuarios"."fechaInicio" AS inicio,
generales."sesionUsuarios"."fechaFin" AS fin

FROM generales."sesionUsuarios"
INNER JOIN operaciones.usuarios ON(generales."sesionUsuarios"."idUsuario" = operaciones.usuarios.id)

     WHERE  DATE(generales."sesionUsuarios"."fechaInicio") BETWEEN '${fechaInicio}'
AND '${fechaFin}'
 
   ORDER BY
operaciones.usuarios."idEmpleado"
) AS "logueosD"

GROUP BY
"logueosD"."idEmpleado","logueosD".inicio,"logueosD".fin
) logueos ON logueos."idEmpleado" = "recursosHumanos".empleado.id


-- ----------------------------------------------------------------------------------------------------------------------------------
--  PAUSAS

LEFT JOIN ( 

SELECT
pausas."idEmpleado" AS "idEmpleado",
count( pausas."idPausa" ) AS "totalPausas",
sum( pausas.tiempo ) AS "totalTiempoPausas"

FROM
(
  SELECT  
operaciones.usuarios."idEmpleado" AS "idEmpleado",
    operaciones."pausaUsuarios".id AS "idPausa",
CASE WHEN
EXTRACT(EPOCH FROM operaciones."pausaUsuarios"."fechaFin"::TIMESTAMP- operaciones."pausaUsuarios"."fechaInicio"::TIMESTAMP)>0
    THEN EXTRACT(EPOCH FROM operaciones."pausaUsuarios"."fechaFin"::TIMESTAMP- operaciones."pausaUsuarios"."fechaInicio"::TIMESTAMP)
ELSE 0 END AS tiempo

FROM operaciones."pausaUsuarios"
LEFT JOIN operaciones."motivosPausa" ON(operaciones."pausaUsuarios"."idMotivosPausa" = operaciones."motivosPausa".id)
    INNER JOIN operaciones.usuarios ON operaciones."pausaUsuarios"."idUsuario"= operaciones.usuarios.id
WHERE DATE(operaciones."pausaUsuarios"."fechaInicio") BETWEEN '${fechaInicio}' 
AND '${fechaFin}'
  ORDER BY
operaciones.usuarios."idEmpleado",operaciones."pausaUsuarios".id

) AS pausas

GROUP BY
pausas."idEmpleado"
ORDER BY
pausas."idEmpleado"
) pausas ON pausas."idEmpleado" = "recursosHumanos".empleado.id 

        `, {
            type: QueryTypes.SELECT
        });
    }

    static async avance(fechaInicio,fechaFin): Promise<any> {
        return await dbPostgres.query(`
        
        `, {
            type: QueryTypes.SELECT
        });
    }

    static async acd(fechaInicio,fechaFin): Promise<any> {
        return await dbPostgres.query(`
SELECT
DISTINCT ON(operaciones.llamadas.id)
llamadas.id,
-- MAX(generales.cdr.disposition ) AS estado,
generales.cdr.disposition  AS estado,
"recursosHumanos".departamento.descripcion AS departamento,
CONCAT_WS( ' ', "recursosHumanos".precandidato.nombre, "recursosHumanos".precandidato."apellidoPaterno","recursosHumanos".precandidato."apellidoMaterno" ) AS ejecutivo,
llamadas.numero,
llamadas."fechaRegistro" AS "fechaLlamada",
llamadas."fechaInicio":: TIME AS "horaInicio",
llamadas."fechaFinal":: TIME AS "horaFinal",
EXTRACT(EPOCH FROM llamadas."fechaFinal"-llamadas."fechaInicio") AS duracion,
operaciones."etiquetaSolicitud".descripcion AS etiqueta,
operaciones."subEtiquetaSolicitud".descripcion AS subEtiqueta,
generales.emisiones.poliza,
generales.emisiones."fechaInicio",
generales."estadoRenovacion".estado AS "estadoEmision"
FROM
operaciones.llamadas
INNER JOIN generales.cdr ON generales.cdr.uniqueid = operaciones.llamadas."idAsterisk"
INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones.llamadas."idEmpleado"
INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos".empleado."idCandidato"
INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos".candidato."idPrecandidato"
LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".empleado."idPlaza"="recursosHumanos".plaza.id
LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento"="recursosHumanos".departamento.id
LEFT JOIN operaciones."subEtiquetaSolicitud" ON operaciones."subEtiquetaSolicitud".id = operaciones.llamadas."idSubEtiqueta"
LEFT JOIN operaciones."etiquetaSolicitud" ON operaciones."subEtiquetaSolicitud"."idEtiquetaSolicitud" = operaciones."etiquetaSolicitud".id
LEFT JOIN generales.emisiones ON generales.emisiones."idRegistro"= operaciones.llamadas."idRegistro"
LEFT JOIN generales."estadoRenovacion" ON generales.emisiones."idEstadoRenovacion" = generales."estadoRenovacion".id 
WHERE
"recursosHumanos".departamento."idArea" IN (21)
  AND operaciones.llamadas."idTipoLlamada"=3  
AND DATE(llamadas."fechaRegistro") BETWEEN '${fechaInicio}' 
AND '${fechaFin}' 
        `, {
            type: QueryTypes.SELECT
        });
    }

    static async out(fechaInicio,fechaFin): Promise<any> {
        return await dbPostgres.query(`
SELECT
DISTINCT ON(operaciones.llamadas.id)
llamadas.id,
-- MAX(generales.cdr.disposition ) AS estado,
generales.cdr.disposition  AS estado,
"recursosHumanos".departamento.descripcion AS departamento,
CONCAT_WS( ' ', "recursosHumanos".precandidato.nombre, "recursosHumanos".precandidato."apellidoPaterno","recursosHumanos".precandidato."apellidoMaterno" ) AS ejecutivo,
llamadas.numero,
llamadas."fechaRegistro" AS "fechaLlamada",
llamadas."fechaInicio":: TIME AS "horaInicio",
llamadas."fechaFinal":: TIME AS "horaFinal",
EXTRACT(EPOCH FROM llamadas."fechaFinal"-llamadas."fechaInicio") AS duracion,
operaciones."etiquetaSolicitud".descripcion AS etiqueta,
operaciones."subEtiquetaSolicitud".descripcion AS subEtiqueta,
generales.emisiones.poliza,
generales.emisiones."fechaInicio",
generales."estadoRenovacion".estado AS "estadoEmision"
FROM
operaciones.llamadas
INNER JOIN generales.cdr ON generales.cdr.uniqueid = operaciones.llamadas."idAsterisk"
INNER JOIN "recursosHumanos".empleado ON "recursosHumanos".empleado.id = operaciones.llamadas."idEmpleado"
INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".candidato.id = "recursosHumanos".empleado."idCandidato"
INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".precandidato.id = "recursosHumanos".candidato."idPrecandidato"
LEFT JOIN "recursosHumanos".plaza ON "recursosHumanos".empleado."idPlaza"="recursosHumanos".plaza.id
LEFT JOIN "recursosHumanos".departamento ON "recursosHumanos".plaza."idDepartamento"="recursosHumanos".departamento.id
LEFT JOIN operaciones."subEtiquetaSolicitud" ON operaciones."subEtiquetaSolicitud".id = operaciones.llamadas."idSubEtiqueta"
LEFT JOIN operaciones."etiquetaSolicitud" ON operaciones."subEtiquetaSolicitud"."idEtiquetaSolicitud" = operaciones."etiquetaSolicitud".id
LEFT JOIN generales.emisiones ON generales.emisiones."idRegistro"= operaciones.llamadas."idRegistro"
LEFT JOIN generales."estadoRenovacion" ON generales.emisiones."idEstadoRenovacion" = generales."estadoRenovacion".id 
WHERE
"recursosHumanos".departamento."idArea" IN (21)
  AND operaciones.llamadas."idTipoLlamada"=4  
AND DATE(llamadas."fechaRegistro") BETWEEN '${fechaInicio}' 
AND '${fechaFin}' 
        `, {
            type: QueryTypes.SELECT
        });
    }
}

import {QueryTypes} from "sequelize";
import{dbPostgres} from "../../configs/connection";

export default class SolicitudesCarruselQueries {
    static async findAllByIdEstadoSolicitudCarrusel(idEstadoSolicitudCarrusel: number): Promise<any> {
        return await dbPostgres.query(`
           SELECT *
               FROM operaciones."solicitudesCarrusel"
               WHERE operaciones."solicitudesCarrusel"."idEstadoSolicitudCarrusel" = ${idEstadoSolicitudCarrusel}
                 ORDER BY
                "idEstadoSolicitudCarrusel" DESC
        `, {
            type: QueryTypes.SELECT
        });
    }
}

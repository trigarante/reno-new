import VisualizacionesGacetaModel from "../../../models/desarrollo-organizacional/trigaranteComunica/visualizacionesGacetaModel";
import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

export default class VisualizacionesGacetaQueries {

    static async create(data: VisualizacionesGacetaModel) {
        return await VisualizacionesGacetaModel.create({...data});
    }

    static async getVisualizacionByIdEmpleado(idEmpleado) {
        return (await dbPostgres.query(`
            SELECT *
            FROM "recursosHumanos"."visualizacionesGaceta" vg
            WHERE "idEmpleado" = ${idEmpleado}
            AND "idGaceta" = (
                SELECT g.id
                FROM "recursosHumanos".gaceta g
                WHERE CURRENT_DATE >= g."fechaLanzamiento" and g.activo = 1
                ORDER BY g."fechaLanzamiento"
                DESC LIMIT 1)
        `, {
            type: QueryTypes.SELECT,
            plain: true
        }));
    }

    static async getByIdGaceta() {
        return (await dbPostgres.query(`
            SELECT
                vg.id AS id,
                vg."idEmpleado" AS "idEmpleado",
                vg."idGaceta" AS "idGaceta",
                vg."fechaRegistro" AS "fechaRegistro",
                p.nombre AS nombre,
                p."apellidoPaterno" AS "apellidoPaterno",
                p."apellidoMaterno" AS "apellidoMaterno"
            FROM "recursosHumanos"."visualizacionesGaceta" vg
                JOIN "recursosHumanos".empleado e ON vg."idEmpleado" = e.id
                JOIN "recursosHumanos".candidato c ON c.id = e."idCandidato"
                JOIN "recursosHumanos".precandidato p ON c."idPrecandidato" = p.id
            WHERE p."idEstadoRH" = 1
        `, {
            type: QueryTypes.SELECT,
        }))
    }

    static async getNoVisualizadas(idGaceta: number) {
        return (await dbPostgres.query(`
            SELECT
                e.id,
                p.nombre,
                p."apellidoMaterno",
                p."apellidoPaterno"
            FROM "recursosHumanos".empleado e
                INNER JOIN "recursosHumanos".candidato c ON c.id = e."idCandidato"
                INNER JOIN "recursosHumanos".precandidato p ON (
                    c."idPrecandidato" = p.id
                    and p."idEstadoRH" = 1)
            WHERE NOT EXISTS (
                    SELECT *
                    FROM "recursosHumanos"."visualizacionesGaceta" vg
                    WHERE vg."idEmpleado" = e.id
                    AND vg."idGaceta" = ${idGaceta})
        `, {
            type: QueryTypes.SELECT,
        }))
    }
}

import db from "../../configs/connection";
import {QueryTypes} from "sequelize";
import TipoPagoModel from "../../models/venta-nueva/solicitudes/steps/registro-poliza/tipoPagoModel";

export default class RegistroPolizaQueries {
    static async findAllByIdSubRamoAndActivo(idSubRamo): Promise<any> {
        return await db.query(`
            SELECT
                productosSocio.id,
                productosSocio.idSubRamo,
                tipoProducto.tipo AS tipoProducto,
                productosSocio.activo
            FROM
                productosSocio
            INNER JOIN tipoProducto ON productosSocio.idTipoProducto = tipoProducto.id
            WHERE
                 productosSocio.idSubRamo = ${idSubRamo} AND
                 productosSocio.activo = 1
        `, {
            type: QueryTypes.SELECT
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async findAllByIdLessThanAndActivo(): Promise<any> {
        return await TipoPagoModel.findAll({
            where: {
                id: [1, 2, 3, 4, 5, 6],
                activo: 1
            }
        });
    }

    static async findRegistroById(idRegistro): Promise<any> {
        return await db.query(`
            SELECT
                registro.id AS id,
                registro.idEmpleado AS idEmpleado,
                registro.idProducto AS idProducto,
                registro.idTipoPago AS idTipoPago,
                registro.idProductoSocio AS idProductoSocio,
                registro.idFlujoPoliza AS idFlujoPoliza,
                flujoPoliza.estado AS flujoPoliza,
                registro.poliza AS poliza,
                registro.fechaInicio AS fechaInicio,
                registro.primaNeta AS primaNeta,
                registro.fechaRegistro AS fechaRegistro,
                registro.archivo AS archivo,
                registro.idPeriodicidad AS idPeriodicidad,
                registro.idSocio AS idSocio,
                periodicidad.nombre AS periodicidad,
                registro.idEstadoPoliza AS idEstadoPoliza,
                estadoPoliza.estado AS estado,
                registro.oficina AS oficina,
                subRamo.idRamo AS idRamo,
                productosSocio.idSubRamo AS idSubRamo
            FROM
                registro
            JOIN productosSocio ON registro.idProductoSocio = productosSocio.id
            JOIN subRamo ON productosSocio.idSubRamo = subRamo.id
            JOIN estadoPoliza ON registro.idEstadoPoliza = estadoPoliza.id
            JOIN flujoPoliza ON flujoPoliza.id = registro.idFlujoPoliza
            JOIN periodicidad ON periodicidad.id = registro.idPeriodicidad

            WHERE
                 registro.id = ${idRegistro}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async buscarPoliza(poliza, idSocio, mes, anio): Promise<any> {
        return await db.query(`
            SELECT
                *
            FROM
                registro
            WHERE
                 registro.poliza = ${poliza} AND
                 registro.idSocio = ${idSocio}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async findAllByIdRegistroAndActivo(idRegistro): Promise<any> {
        return await db.query(`
            SELECT
                recibos.id AS id,
                recibos.idRegistro AS idRegistro,
                recibos.numero AS numero,
                recibos.cantidad AS cantidad,
                recibos.activo AS activo
            FROM
                recibos
            WHERE
                 recibos.idRegistro = ${idRegistro}
        `, {
            type: QueryTypes.SELECT
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }
}

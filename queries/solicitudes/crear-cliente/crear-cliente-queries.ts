import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

export default class CrearClienteQueries {

    static async getColoniaByCp(cp: any): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                *
            FROM
                operaciones."cpSepomex"
            WHERE
                operaciones."cpSepomex".cp = ${cp}
        `, {
            type: QueryTypes.SELECT,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    static async getCapitalesByCp(cp: any): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                *
            FROM
                operaciones."cpPeru"
            WHERE
                operaciones."cpPeru".cp = ${cp}
        `, {
            type: QueryTypes.SELECT,
        }).catch( e => {
            console.log(e, 'eeeeeeeeeeeeeeeeeeeeeeeeeeeee');
        });
    }

    // static async updateCliente(idCliente: number, data: ClienteModel): Promise<any> {
    //     return await ClienteModel.update({...data}, {
    //         where: {
    //             id: idCliente,
    //         }
    //     });
    // }
}

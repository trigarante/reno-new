import CarrierModel from "../../models/venta-nueva/carrierTarjetas/CarrierModel";


export default class CarrierQueries {

    static async findAll(): Promise<any> {
        return await CarrierModel.findAll({
            where: {activo: 1}
        })
    }

}




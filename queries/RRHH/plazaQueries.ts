import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";

export default class PlazaQueries {
    static async getPlazasHijo(id) {
        return await dbPostgres.query(`
        SELECT empleado.id
        FROM "recursosHumanos".empleado
        JOIN "recursosHumanos".plaza ON plaza.id = empleado."idPlaza"
        JOIN "recursosHumanos".candidato ON candidato.id = empleado."idCandidato"
        JOIN "recursosHumanos".precandidato ON precandidato.id = candidato."idPrecandidato"
        WHERE
            plaza."idPadre" = ${id}
            and empleado.activo =1
            and precandidato."idEstadoRH" =  1
       `, {
            type: QueryTypes.SELECT

        })
    }
}

import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";

export default class RegistroExternoQueries {
    static async get() {
        return dbPostgres.query(`
        SELECT
            "registroExterno".id,
            "usuarioApp".correo "idUsuarioApp",
            "registroExterno".nombre,
            "registroExterno".poliza,
            "registroExterno".alias,
            "registroExterno"."fechaRegistro",
            "registroExterno"."fechaVencimiento",
            socios.alias,
            "tipoSeguro".descripcion
        FROM operaciones."registroExterno"
        LEFT JOIN operaciones.socios ON socios.id = "registroExterno"."idSocio"
        LEFT JOIN operaciones."tipoSeguro" ON "tipoSeguro".id = "registroExterno"."idTipoSeguro"
        LEFT JOIN app."usuarioApp" ON "usuarioApp".id = "registroExterno"."idUsuarioApp"
        ORDER BY id DESC`, {
            type: QueryTypes.SELECT,
        })
    }
}

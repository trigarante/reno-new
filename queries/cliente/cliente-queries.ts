import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";
import ClienteModel from "../../models/venta-nueva/cliente/clienteModel";

export default class ClienteQueries {

    static async clienteAll(id): Promise<any> {
        return await dbPostgres.query(`
            SELECT
                c.id,
                c."idPais",
                c.nombre,
                c.paterno,
                c.materno,
                c.cp,
                c.calle,
                c."numInt",
                c."numExt",
                c."idColonia",
                cp.asenta AS colonia,
                c.genero,
                c."telefonoFijo",
                c."telefonoMovil",
                c.correo,
                c."fechaNacimiento",
                c.curp,
                c.rfc,
                c."fechaRegistro",
                c.archivo,
                p.nombre AS "nombrePaises",
                c."razonSocial",
                c."archivoSubido"
            FROM operaciones.cliente c
            LEFT JOIN operaciones.paises p ON c."idPais" = p.id
            LEFT JOIN operaciones."cpSepomex" cp ON c."idColonia" = cp."idCp"
            where c.id = ${id}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        })
    }
    static async dniCliente (dni) {
        return await dbPostgres.query(`
            SELECT cl.id, cl."idPais", cl.nombre, cl.paterno, cl.materno, cl.cp, cl.calle,
            cl."numInt", cl."numExt", cl."idColoniaPeru", cpp.capitales, cl.genero, cl."telefonoFijo",
            cl."telefonoMovil", cl.correo, cl."fechaNacimiento", cl.dni, cl.ruc, cl."fechaRegistro", cl.archivo,
            pa.nombre as "nombrePaises", cl."razonSocial", cl."archivoSubido"
            FROM operaciones.cliente cl
            INNER JOIN operaciones.paises pa
            on cl."idPais" = pa."id"
            LEFT JOIN operaciones."cpPeru" cpp
            ON cpp."id" = cl."idColoniaPeru"
            WHERE cl.dni = '${dni}'
            ORDER BY cl."id"DESC
        `, {
            type: QueryTypes.SELECT,
            plain: true
        });
    }

    static async getByRegistro(idRegistro) {
        return await dbPostgres.query(`
        SELECT
            cliente.id,
            operaciones.cliente."telefonoMovil" "numeroMovil",
            operaciones.cliente.correo,
            operaciones.cliente.nombre || ' ' || operaciones.cliente.paterno || ' ' || operaciones.cliente.materno AS "nombre"
        FROM
            operaciones.registro
        INNER JOIN
            operaciones."productoCliente"
            ON
            operaciones.registro."idProducto" = operaciones."productoCliente"."id"
        INNER JOIN
            operaciones.cliente
            ON
            operaciones."productoCliente"."idCliente" = operaciones.cliente."id"
        Where
            registro."id" = ${idRegistro}
        `, {
            type: QueryTypes.SELECT,
            plain: true
        });
    }

    static async getByCurp(curp) {
        return await ClienteModel.findOne({
            where: {curp}
        })
    }

    static async getByRFC(rfc) {
        return await ClienteModel.findOne({
            where: {rfc}
        })
    }

    static async post(data) {
        return await ClienteModel.create(data).then(data => {
            return data['id'];
        }).catch(err => console.log(err))
    }

    static async update(data, id) {
        return await ClienteModel.update(data, {
            where: {id},
        }).catch(err => console.log(err))
    }
}




import RamoModel from "../../models/venta-nueva/ramo/ramoModel";
import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";

export default class RamoQueries {
    static async getByIdSocio(idSocio) {
        return await  dbPostgres.query(`
        SELECT operaciones.ramo.id,  operaciones.ramo."idSocio", operaciones."tipoRamo".tipo AS "tipoRamo" FROM operaciones.ramo
        INNER JOIN operaciones."tipoRamo" ON operaciones."tipoRamo".id = operaciones.ramo."idTipoRamo"
        WHERE operaciones.ramo."idSocio" = '${idSocio}' AND operaciones."ramo".activo =1`, {
            type: QueryTypes.SELECT,
        })
    }
}

import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";
import AsistenciasModel from "../../../models/administracion-personal/asistencias/asistenciasModel";

export default class AsistenciasService {
    static async encontrarDuplicadoUltimoMovi(id) {
        return await AsistenciasModel.findOne({
            where: {
                solicitud: true, id
            },
            raw: true
        });
    }
    static async traerAsistencia(id) {
        return await AsistenciasModel.findOne({
            where: {
                id
            },
            // raw: true
        });
    }
    static async postMulti(data) {
        return await AsistenciasModel.bulkCreate(data);
    }

    static async post(data) {
        return await AsistenciasModel.create(data);
    }

    static async update(idAsistencia: number, data) {
        return await AsistenciasModel.update({...data},
            {
                where: {
                    id: idAsistencia
                }
            })
    }

    static async verAsistencias(inicio, fin) {
        return await dbPostgres.query(`
        SELECT * FROM "recursosHumanos".asistencias_1('${inicio}', '${fin}')
        `, {
            type: QueryTypes.SELECT
        })
    }

    static async obtenerEmpleados(idDepartamento, idArea) {
        return await dbPostgres.query(`
        SELECT
            e."id",
            pre.nombre || ' ' || pre."apellidoPaterno" || ' ' || pre."apellidoMaterno" "nombre",
            d."id" "idDepartamento"
        FROM
            "recursosHumanos".empleado e
            INNER JOIN "recursosHumanos".candidato cd ON e."idCandidato" = cd."id"
            INNER JOIN "recursosHumanos".precandidato pre ON cd."idPrecandidato" = pre."id"
            INNER JOIN "recursosHumanos".plaza pl ON e."idPlaza" = pl."id"
            INNER JOIN "recursosHumanos".departamento d ON pl."idDepartamento" = d."id"
        WHERE
            e.activo = 1 AND
            e."idPlaza" IS NOT NULL
            ${idDepartamento}
            ${idArea}
        `, {
            type: QueryTypes.SELECT
        })
    }
    static async todosEmpleados() {
        return await dbPostgres.query(`
        SELECT
            e."id",
            pre.nombre || ' ' || pre."apellidoPaterno" || ' ' || pre."apellidoMaterno" "nombre",
            d."id" "idDepartamento"
        FROM
            "recursosHumanos".empleado e
            INNER JOIN "recursosHumanos".candidato cd ON e."idCandidato" = cd."id"
            INNER JOIN "recursosHumanos".precandidato pre ON cd."idPrecandidato" = pre."id"
            INNER JOIN "recursosHumanos".plaza pl ON e."idPlaza" = pl."id"
            INNER JOIN "recursosHumanos".departamento d ON pl."idDepartamento" = d."id"
        WHERE
            e.activo = 1 AND
            e."idPlaza" IS NOT NULL
        `, {
            type: QueryTypes.SELECT
        })
    }

    static async obtenerEmpleadoPlaza(idEmpleado) {
        return await dbPostgres.query(`
        SELECT
            pl."id"
        FROM
            "recursosHumanos".empleado e
            INNER JOIN "recursosHumanos".candidato cd ON e."idCandidato" = cd."id"
            INNER JOIN "recursosHumanos".precandidato pre ON cd."idPrecandidato" = pre."id"
            INNER JOIN "recursosHumanos".plaza pl ON e."idPlaza" = pl."id"
            INNER JOIN "recursosHumanos".departamento d ON pl."idDepartamento" = d."id"
        WHERE
            e.activo = 1 AND
            e."idPlaza" IS NOT NULL AND
            e.id = ${idEmpleado}
        `, {
            plain: true,
            type: QueryTypes.SELECT
        })
    }
    static async siAsistieron() {
        return await dbPostgres.query(`
        SELECT
            e."id"
        FROM
			"recursosHumanos"."asistencias"
			LEFT JOIN "recursosHumanos".empleado e ON e.id = asistencias."idEmpleado"
			LEFT JOIN "recursosHumanos"."estadoAsistencia" ON "estadoAsistencia".id = asistencias."idEstadoAsistencia"
            INNER JOIN "recursosHumanos".candidato cd ON e."idCandidato" = cd."id"
            INNER JOIN "recursosHumanos".precandidato pre ON cd."idPrecandidato" = pre."id"
            INNER JOIN "recursosHumanos".plaza pl ON e."idPlaza" = pl."id"
            INNER JOIN "recursosHumanos".departamento d ON pl."idDepartamento" = d."id"
		WHERE
            e.activo = 1 AND
            e."idPlaza" IS NOT NULL and
			d.id = 3 and
			DATE(asistencias."fechaRegistro") = DATE('2022-04-03')
        `, {
            type: QueryTypes.SELECT
        })
    }
    static async noAsistieron(ids: number[]) {
        return await dbPostgres.query(`
        select
            e.id
        from
            "recursosHumanos".empleado e
            INNER JOIN "recursosHumanos".candidato cd ON e."idCandidato" = cd."id"
            INNER JOIN "recursosHumanos".precandidato pre ON cd."idPrecandidato" = pre."id"
            INNER JOIN "recursosHumanos".plaza pl ON e."idPlaza" = pl."id"
            INNER JOIN "recursosHumanos".departamento d ON pl."idDepartamento" = d."id"
       where
            e.activo = 1 AND
            e."idPlaza" IS NOT NULL and
            d.id = 3 and
            e.id not in (${ids})
        `, {
            type: QueryTypes.SELECT
        })
    }
}

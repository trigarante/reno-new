import LogsAdpModel from "../../../models/administracion-personal/logs/logsAdpModel";

export default class LogsAdpService {
    static async create(data) {
        return await LogsAdpModel.create(data).catch(err => {
            console.log(err)
        })
    }
}

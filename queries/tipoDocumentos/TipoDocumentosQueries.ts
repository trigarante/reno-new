import TipoDocumentosModel from "../../models/venta-nueva/tipoDocumentos/TipoDocumentosModel";

export default class TipoDocumentosQueries {
    static async getByIdTablaAndActivos(idTabla) {
        return await TipoDocumentosModel.findAll({
            where: {
                activo: 1,
                idTabla,
            },
        })
    }
}

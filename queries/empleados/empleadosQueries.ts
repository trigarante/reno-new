import EmpleadosModel from "../../models/venta-nueva/empleados/EmpleadosModel";
import {dbPostgres} from "../../configs/connection";
import {QueryTypes} from "sequelize";

export default class EmpleadosQueries {
    static async getById(id) {
        return await EmpleadosModel.findOne({
            where: {
                id
            },
        })
    }


    // Queries de empleados desarrollo organizacional

    static async getCorreoPersonal(idEmpleado){
        return await dbPostgres.query(`
            SELECT p.email
            FROM "recursosHumanos".empleado e
                JOIN "recursosHumanos".candidato c on e."idCandidato" = c.id
                JOIN "recursosHumanos".precandidato p on c."idPrecandidato" = p.id
            WHERE
            e.id = ${idEmpleado}
            `,{
            type: QueryTypes.SELECT,
            plain: true
        })

    }

    // FIN

    static async getEmpleadosByIdDepartamento(...idDepartamento){
        return await dbPostgres.query(`
            SELECT
                empleado.id,
                precandidato.nombre || ' ' || precandidato."apellidoPaterno" || ' ' || precandidato."apellidoMaterno" as nombre
            FROM
                "recursosHumanos".departamento
                INNER JOIN "recursosHumanos".plaza ON departamento.id = "plaza"."idDepartamento"
                INNER JOIN "recursosHumanos".empleado ON plaza."id" = "empleado"."idPlaza"
                INNER JOIN "recursosHumanos".candidato ON candidato.id = "empleado"."idCandidato"
                INNER JOIN "recursosHumanos".precandidato ON precandidato.id = "candidato"."idPrecandidato"
                INNER JOIN operaciones.usuarios ON departamento."id" = usuarios."idDepartamento" AND usuarios."idEmpleado" = empleado."id"
            WHERE
                departamento."id" IN (${idDepartamento})
                AND empleado.activo = 1
                AND plaza."idPuesto" = 7
                AND usuarios.usuario IS NOT NULL
                AND precandidato."idEstadoRH" = 1
            `,{
            type: QueryTypes.SELECT,
        })
    }
}

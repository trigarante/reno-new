import {QueryTypes} from "sequelize";
import {dbPostgres} from "../../configs/connection";

export default class EmisionesQueries {

    static async getPermisosVisualizacion(idEmpleado: number): Promise<any> {
        return await dbPostgres.query(`
            SELECT gu."idPermisosVisualizacion" as "idPermisoVisualizacion",
            pl."idDepartamento",
            u.id AS "idUsuario",
            pl."idPuesto",
            pl.id
            FROM operaciones.usuarios u
            INNER JOIN generales."grupoUsuarios" gu ON gu.id = u."idGrupo"
            INNER JOIN "recursosHumanos".empleado e ON e.id = u."idEmpleado"
            INNER JOIN "recursosHumanos".plaza pl ON pl.id = e."idPlaza"
            WHERE e.id = ${idEmpleado}
        `, {
            type: QueryTypes.SELECT,
            plain: true,
        });
    }
    static async getAll(fechaInicio, fechaFin, idTipoBandeja): Promise<any> {
        return await dbPostgres.query(`
                    SELECT
                    DISTINCT ON (generales.emisiones.id)
                    generales.emisiones.id,
                    generales.emisiones."idRegistro",
                    generales.emisiones."idEmpleado",
                    generales.emisiones."idTipoPago",
                    generales.emisiones."idEstadoRenovacion",
                    generales.emisiones."idProductoSocio",
                    generales.emisiones."idSocio",
                    operaciones.registro."idProducto" AS "idProductoCliente",
                    operaciones."subRamo".id AS "idSubramo",
                    operaciones.ramo.id AS "idRamo",
                    generales.emisiones.poliza AS poliza,
                    generales.emisiones."fechaInicio",
                    generales.emisiones."primaNeta",
                    generales.emisiones."montoSubsecuente",
                    operaciones.registro."primaNeta" AS "primaNetaAnterior",
                    ((
                    generales.emisiones."primaNeta" / (CASE WHEN operaciones.registro."primaNeta" = 0 THEN 1 ELSE operaciones.registro."primaNeta" END)
                    ) - 1
                    ) AS variacion,
                    generales.emisiones."fechaRegistro",
                    generales.emisiones.archivo,
                    generales.emisiones."idTipoRenovacion",
                    generales.emisiones."idPeriodicidad",
                    generales.emisiones."primerPago",
                    operaciones."tipoPago"."tipoPago",
                    generales."estadoRenovacion".estado AS "estadoRenovacion",
                    operaciones.socios."nombreComercial",
                    operaciones.cliente."archivoSubido",
                    operaciones.registro."idFlujoPoliza",
                    operaciones.registro.poliza AS "polizaVieja",
                    operaciones.registro."archivo"AS "archivoViejo",
                    operaciones."productoCliente"."idCliente",
                    operaciones.recibos."idEstadoRecibos",
                    operaciones.pagos.id AS "idPago",
                    generales.emisiones."idTipoBandejaRenovacion",
                    "nuevo"."idNuevoProducto" AS "idNuevoProducto",
                    "nuevo"."idNuevoRegistro" AS "idNuevoRegistro",
                    "nuevo"."idNuevoRecibo" AS "idNuevoRecibo",
                    "nuevo"."idNuevoPago" AS "idNuevoPago",
                    nuevo."idEstadoIntentoPago",
                    operaciones."productoCliente"."idSolicitud",
                    operaciones.cliente.correo AS correo,
                    operaciones.cliente."telefonoMovil",
                    operaciones.cliente.nombre AS "nombreCliente",
                    operaciones.cliente.paterno AS "paternoCliente",
                    operaciones.cliente.materno AS "maternoCliente",
                    operaciones.cliente.rfc,
                    "recursosHumanos".precandidato.nombre AS "nombreEmpleadoRenovaciones",
                    "recursosHumanos".precandidato."apellidoPaterno" AS "apellidoPaternoEmpleadoRenovaciones",
                    "recursosHumanos".precandidato."apellidoMaterno" AS "apellidoMaternoEmpleadoRenovaciones",
                    generales.emisiones."primaTotal"
                    FROM
                    generales.emisiones
                    JOIN operaciones."tipoPago" ON operaciones."tipoPago".id = generales.emisiones."idTipoPago"
                    JOIN generales."estadoRenovacion" ON generales.emisiones."idEstadoRenovacion" = generales."estadoRenovacion".id
                    JOIN operaciones."productosSocio" ON generales.emisiones."idProductoSocio" = operaciones."productosSocio".id
                    JOIN operaciones."subRamo" ON operaciones."productosSocio"."idSubRamo" = operaciones."subRamo".id
                    JOIN operaciones.ramo ON operaciones."subRamo"."idRamo" = operaciones.ramo.id
                    JOIN operaciones.socios ON generales.emisiones."idSocio" = operaciones.socios.id
                    JOIN operaciones.registro ON operaciones.registro.id = generales.emisiones."idRegistro"
                    JOIN operaciones."productoCliente" ON registro."idProducto" = operaciones."productoCliente".id
                    JOIN operaciones.cliente ON operaciones."productoCliente"."idCliente" = operaciones.cliente.id
                    LEFT JOIN "recursosHumanos".empleado ON generales.emisiones."idEmpleadoRenovacion" = "recursosHumanos".empleado.id
                    LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato.id
                    LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id
                    LEFT JOIN operaciones.recibos ON operaciones.recibos."idRegistro" = operaciones.registro.id
                    LEFT JOIN operaciones.pagos ON operaciones.pagos."idRecibo" = operaciones.recibos.id
                    LEFT JOIN (
                    SELECT
                    generales.emisiones."id",
                    "nuevoProductoCliente".id AS "idNuevoProducto",
                    "registroNuevo".id AS "idNuevoRegistro",
                    "recibosNuevos".id AS "idNuevoRecibo",
                    "pagosNuevo".id AS "idNuevoPago",
                    generales."intentosPago"."idEstadoIntentoPago"
                    FROM generales.emisiones
                    INNER JOIN operaciones."productoCliente" "nuevoProductoCliente" ON generales.emisiones.id = "nuevoProductoCliente"."idEmision"
                    INNER JOIN operaciones.registro "registroNuevo" ON "nuevoProductoCliente".id = "registroNuevo"."idProducto"
                    INNER JOIN operaciones.recibos "recibosNuevos" ON "registroNuevo".id = "recibosNuevos"."idRegistro"
                    LEFT JOIN operaciones.pagos "pagosNuevo" ON "recibosNuevos".id = "pagosNuevo"."idRecibo"
                    LEFT JOIN generales."intentosPago" ON "recibosNuevos".id = "generales"."intentosPago"."idRecibo"
                    WHERE "recibosNuevos".numero = 1
                    AND generales.emisiones."fechaInicio" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
                    ) nuevo ON nuevo."id" = generales.emisiones."id"
                    WHERE
                    generales.emisiones."idTipoBandejaRenovacion" = '${idTipoBandeja}'
                    AND generales.emisiones."fechaInicio" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
                    ORDER BY generales.emisiones.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAll2(fechaInicio, fechaFin): Promise<any> {
        return await dbPostgres.query(`
SELECT
DISTINCT ON (generales.emisiones.id)
generales.emisiones.id,
generales.emisiones."idRegistro",
generales.emisiones."idEmpleado",
generales.emisiones."idTipoPago",
generales.emisiones."idEstadoRenovacion",
generales.emisiones."idProductoSocio",
generales.emisiones."idSocio",
operaciones.registro."idProducto" AS "idProductoCliente",
operaciones."subRamo".id AS "idSubramo",
operaciones.ramo.id AS "idRamo",
generales.emisiones.poliza AS poliza,
generales.emisiones."fechaInicio",
generales.emisiones."primaNeta",
generales.emisiones."montoSubsecuente",
operaciones.registro."primaNeta" AS "primaNetaAnterior",
((
generales.emisiones."primaNeta" / (CASE WHEN operaciones.registro."primaNeta" = 0 THEN 1 ELSE operaciones.registro."primaNeta" END)
) - 1 
) AS variacion,
generales.emisiones."fechaRegistro",
generales.emisiones.archivo,
generales.emisiones."idTipoRenovacion",
generales.emisiones."idPeriodicidad",
generales.emisiones."primerPago",
operaciones."tipoPago"."tipoPago",
generales."estadoRenovacion".estado AS "estadoRenovacion",
operaciones.socios."nombreComercial",
operaciones.cliente."archivoSubido",
operaciones.registro."idFlujoPoliza",
operaciones.registro.poliza AS "polizaVieja",
operaciones.registro."archivo"AS "archivoViejo",
operaciones."productoCliente"."idCliente",
operaciones.recibos."idEstadoRecibos",
operaciones.pagos.id AS "idPago",
generales.emisiones."idTipoBandejaRenovacion",
"nuevoProductoCliente".id AS "idNuevoProducto",
"registroNuevo".id AS "idNuevoRegistro",
"recibosNuevos".id AS "idNuevoRecibo",
"pagosNuevo".id AS "idNuevoPago",
generales."intentosPago"."idEstadoIntentoPago",
operaciones."productoCliente"."idSolicitud",
operaciones.cliente.correo AS correo,
operaciones.cliente."telefonoMovil",
operaciones.cliente.nombre AS "nombreCliente",
operaciones.cliente.paterno AS "paternoCliente",
operaciones.cliente.materno AS "maternoCliente",
"recursosHumanos".precandidato.nombre AS "nombreEmpleadoRenovaciones",
"recursosHumanos".precandidato."apellidoPaterno" AS "apellidoPaternoEmpleadoRenovaciones",
"recursosHumanos".precandidato."apellidoMaterno" AS "apellidoMaternoEmpleadoRenovaciones",
CASE WHEN generales.tipificacion.tipo is null THEN 'SIN TIPIFICAR' ELSE generales.tipificacion.tipo end as "tipificacion",
generales.emisiones."primaTotal",
CASE WHEN generales."etiquetaEmision".estado is null THEN 'SIN ETIQUETA EMISION' ELSE generales."etiquetaEmision".estado END as "etiqueta"
FROM
generales.emisiones
JOIN operaciones."tipoPago" ON operaciones."tipoPago".id = generales.emisiones."idTipoPago"
JOIN generales."estadoRenovacion" ON generales.emisiones."idEstadoRenovacion" = generales."estadoRenovacion".id
JOIN operaciones."productosSocio" ON generales.emisiones."idProductoSocio" = operaciones."productosSocio".id
JOIN operaciones."subRamo" ON operaciones."productosSocio"."idSubRamo" = operaciones."subRamo".id
JOIN operaciones.ramo ON operaciones."subRamo"."idRamo" = operaciones.ramo.id
JOIN operaciones.socios ON generales.emisiones."idSocio" = operaciones.socios.id
JOIN operaciones.registro ON operaciones.registro.id = generales.emisiones."idRegistro"
JOIN operaciones."productoCliente" ON registro."idProducto" = operaciones."productoCliente".id
JOIN operaciones.cliente ON operaciones."productoCliente"."idCliente" = operaciones.cliente.id
LEFT JOIN "recursosHumanos".empleado ON generales.emisiones."idEmpleadoRenovacion" = "recursosHumanos".empleado.id
LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato.id
LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id
LEFT JOIN operaciones.recibos ON operaciones.recibos."idRegistro" = operaciones.registro.id
LEFT JOIN operaciones.pagos ON operaciones.pagos."idRecibo" = operaciones.recibos.id
LEFT JOIN operaciones."productoCliente" "nuevoProductoCliente" ON generales.emisiones.id = "nuevoProductoCliente"."idEmision"
LEFT JOIN operaciones.registro "registroNuevo" ON "nuevoProductoCliente".id = "registroNuevo"."idProducto"
LEFT JOIN operaciones.recibos "recibosNuevos" ON "registroNuevo".id = "recibosNuevos"."idRegistro"
LEFT JOIN operaciones.pagos "pagosNuevo" ON "recibosNuevos".id = "pagosNuevo"."idRecibo"
LEFT JOIN generales."intentosPago" ON "recibosNuevos".id = "generales"."intentosPago"."idRecibo" 
LEFT JOIN generales.tipificacion ON generales.tipificacion.id = generales.emisiones."idTipificacion"
LEFT JOIN generales."etiquetaEmision" ON generales."etiquetaEmision"."id" = emisiones."idEtiquetaEmision"
WHERE
generales.emisiones."fechaInicio" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
ORDER BY generales.emisiones.id DESC
        `, {
            type: QueryTypes.SELECT,
        });
    }

    static async getAllByDepartamentoAndIdEmpleado(idDepartamento, idEmpleado, fechaInicio, fechaFin, idTipoBandeja): Promise<any> {
        return await dbPostgres.query(`
         SELECT
DISTINCT ON (generales.emisiones.id)
generales.emisiones.id,
generales.emisiones."idRegistro",
generales.emisiones."idEmpleado",
generales.emisiones."idTipoPago",
generales.emisiones."idEstadoRenovacion",
generales.emisiones."idProductoSocio",
generales.emisiones."idSocio",
operaciones.registro."idProducto" AS "idProductoCliente",
operaciones."subRamo".id AS "idSubramo",
operaciones.ramo.id AS "idRamo",
generales.emisiones.poliza AS poliza,
generales.emisiones."fechaInicio",
generales.emisiones."primaNeta",
generales.emisiones."montoSubsecuente",
operaciones.registro."primaNeta",
((
generales.emisiones."primaNeta" / (CASE WHEN operaciones.registro."primaNeta" = 0 THEN 1 ELSE operaciones.registro."primaNeta" END)
) - 1 
) AS variacion,
generales.emisiones."fechaRegistro",
generales.emisiones.archivo,
generales.emisiones."idTipoRenovacion",
generales.emisiones."idPeriodicidad",
generales.emisiones."primerPago",
operaciones."tipoPago"."tipoPago",
generales."estadoRenovacion".estado AS "estadoRenovacion",
operaciones.socios."nombreComercial",
operaciones.cliente."archivoSubido",
operaciones.registro."idFlujoPoliza",
operaciones.registro.poliza AS "polizaVieja",
operaciones.registro."archivo"AS "archivoViejo",
operaciones."productoCliente"."idCliente",
operaciones.recibos."idEstadoRecibos",
operaciones.pagos.id AS "idPago",
generales.emisiones."idTipoBandejaRenovacion",
"nuevoProductoCliente".id AS "idNuevoProducto",
"registroNuevo".id AS "idNuevoRegistro",
"recibosNuevos".id AS "idNuevoRecibo",
"pagosNuevo".id AS "idNuevoPago",
generales."intentosPago"."idEstadoIntentoPago",
operaciones."productoCliente"."idSolicitud",
operaciones.cliente.correo AS correo,
operaciones.cliente."telefonoMovil",
operaciones.cliente.nombre AS "nombreCliente",
operaciones.cliente.paterno AS "paternoCliente",
operaciones.cliente.materno AS "maternoCliente",
"recursosHumanos".precandidato.nombre AS "nombreEmpleadoRenovaciones",
"recursosHumanos".precandidato."apellidoPaterno" AS "apellidoPaternoEmpleadoRenovaciones",
"recursosHumanos".precandidato."apellidoMaterno" AS "apellidoMaternoEmpleadoRenovaciones" 
FROM
generales.emisiones
JOIN operaciones."tipoPago" ON operaciones."tipoPago".id = generales.emisiones."idTipoPago"
JOIN generales."estadoRenovacion" ON generales.emisiones."idEstadoRenovacion" = generales."estadoRenovacion".id
JOIN operaciones."productosSocio" ON generales.emisiones."idProductoSocio" = operaciones."productosSocio".id
JOIN operaciones."subRamo" ON operaciones."productosSocio"."idSubRamo" = operaciones."subRamo".id
JOIN operaciones.ramo ON operaciones."subRamo"."idRamo" = operaciones.ramo.id
JOIN operaciones.socios ON generales.emisiones."idSocio" = operaciones.socios.id
JOIN operaciones.registro ON operaciones.registro.id = generales.emisiones."idRegistro"
JOIN operaciones."productoCliente" ON registro."idProducto" = operaciones."productoCliente".id
JOIN operaciones.cliente ON operaciones."productoCliente"."idCliente" = operaciones.cliente.id
LEFT JOIN "recursosHumanos".empleado ON generales.emisiones."idEmpleadoRenovacion" = "recursosHumanos".empleado.id
LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato.id
LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id
LEFT JOIN operaciones.recibos ON operaciones.recibos."idRegistro" = operaciones.registro.id
LEFT JOIN operaciones.pagos ON operaciones.pagos."idRecibo" = operaciones.recibos.id
LEFT JOIN operaciones."productoCliente" "nuevoProductoCliente" ON generales.emisiones.id = "nuevoProductoCliente"."idEmision"
LEFT JOIN operaciones.registro "registroNuevo" ON "nuevoProductoCliente".id = "registroNuevo"."idProducto"
LEFT JOIN operaciones.recibos "recibosNuevos" ON "registroNuevo".id = "recibosNuevos"."idRegistro"
LEFT JOIN operaciones.pagos "pagosNuevo" ON "recibosNuevos".id = "pagosNuevo"."idRecibo"
LEFT JOIN generales."intentosPago" ON "recibosNuevos".id = "generales"."intentosPago"."idRecibo" 
WHERE
generales.emisiones."idTipoBandejaRenovacion" = '${idTipoBandeja}'
AND generales.emisiones."fechaInicio" BETWEEN ('${fechaInicio}') AND ('${fechaFin}')
AND "recursosHumanos".empleado.id = ${idEmpleado}
ORDER BY generales.emisiones.id DESC
        `, {
            type: QueryTypes.SELECT,
        })
    }
    static async getAllById(id): Promise<any> {
        return await dbPostgres.query(`
         SELECT
DISTINCT ON (generales.emisiones.id)
generales.emisiones.id,
generales.emisiones."idRegistro",
generales.emisiones."idEmpleado",
generales.emisiones."idTipoPago",
generales.emisiones."idEstadoRenovacion",
generales.emisiones."idProductoSocio",
generales.emisiones."idSocio",
operaciones.registro."idProducto" AS "idProductoCliente",
operaciones."subRamo".id AS "idSubramo",
operaciones.ramo.id AS "idRamo",
generales.emisiones.poliza AS poliza,
generales.emisiones."fechaInicio",
generales.emisiones."primaNeta",
generales.emisiones."montoSubsecuente",
operaciones.registro."primaNeta",
((
generales.emisiones."primaNeta" / (CASE WHEN operaciones.registro."primaNeta" = 0 THEN 1 ELSE operaciones.registro."primaNeta" END)
) - 1 
) AS variacion,
generales.emisiones."fechaRegistro",
generales.emisiones.archivo,
generales.emisiones."idTipoRenovacion",
generales.emisiones."idPeriodicidad",
generales.emisiones."primerPago",
operaciones."tipoPago"."tipoPago",
generales."estadoRenovacion".estado AS "estadoRenovacion",
operaciones.socios."nombreComercial",
operaciones.cliente."archivoSubido",
operaciones.registro."idFlujoPoliza",
operaciones.registro.poliza AS "polizaVieja",
operaciones.registro."archivo"AS "archivoViejo",
operaciones."productoCliente"."idCliente",
operaciones.recibos."idEstadoRecibos",
operaciones.pagos.id AS "idPago",
generales.emisiones."idTipoBandejaRenovacion",
"nuevoProductoCliente".id AS "idNuevoProducto",
"registroNuevo".id AS "idNuevoRegistro",
"recibosNuevos".id AS "idNuevoRecibo",
"pagosNuevo".id AS "idNuevoPago",
generales."intentosPago"."idEstadoIntentoPago",
operaciones."productoCliente"."idSolicitud",
operaciones.cliente.correo AS correo,
operaciones.cliente."telefonoMovil",
operaciones.cliente.nombre AS "nombreCliente",
operaciones.cliente.paterno AS "paternoCliente",
operaciones.cliente.materno AS "maternoCliente",
"recursosHumanos".precandidato.nombre AS "nombreEmpleadoRenovaciones",
"recursosHumanos".precandidato."apellidoPaterno" AS "apellidoPaternoEmpleadoRenovaciones",
"recursosHumanos".precandidato."apellidoMaterno" AS "apellidoMaternoEmpleadoRenovaciones" 
FROM
generales.emisiones
JOIN operaciones."tipoPago" ON operaciones."tipoPago".id = generales.emisiones."idTipoPago"
JOIN generales."estadoRenovacion" ON generales.emisiones."idEstadoRenovacion" = generales."estadoRenovacion".id
JOIN operaciones."productosSocio" ON generales.emisiones."idProductoSocio" = operaciones."productosSocio".id
JOIN operaciones."subRamo" ON operaciones."productosSocio"."idSubRamo" = operaciones."subRamo".id
JOIN operaciones.ramo ON operaciones."subRamo"."idRamo" = operaciones.ramo.id
JOIN operaciones.socios ON generales.emisiones."idSocio" = operaciones.socios.id
JOIN operaciones.registro ON operaciones.registro.id = generales.emisiones."idRegistro"
JOIN operaciones."productoCliente" ON registro."idProducto" = operaciones."productoCliente".id
JOIN operaciones.cliente ON operaciones."productoCliente"."idCliente" = operaciones.cliente.id
LEFT JOIN "recursosHumanos".empleado ON generales.emisiones."idEmpleadoRenovacion" = "recursosHumanos".empleado.id
LEFT JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato.id
LEFT JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id
LEFT JOIN operaciones.recibos ON operaciones.recibos."idRegistro" = operaciones.registro.id
LEFT JOIN operaciones.pagos ON operaciones.pagos."idRecibo" = operaciones.recibos.id
LEFT JOIN operaciones."productoCliente" "nuevoProductoCliente" ON generales.emisiones.id = "nuevoProductoCliente"."idEmision"
LEFT JOIN operaciones.registro "registroNuevo" ON "nuevoProductoCliente".id = "registroNuevo"."idProducto"
LEFT JOIN operaciones.recibos "recibosNuevos" ON "registroNuevo".id = "recibosNuevos"."idRegistro"
LEFT JOIN operaciones.pagos "pagosNuevo" ON "recibosNuevos".id = "pagosNuevo"."idRecibo"
LEFT JOIN generales."intentosPago" ON "recibosNuevos".id = "generales"."intentosPago"."idRecibo" 
WHERE
generales.emisiones.id = '${id}'
ORDER BY generales.emisiones.id DESC
        `, {
            type: QueryTypes.SELECT,
        })
    }

}

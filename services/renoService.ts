import { dbPostgres } from "../configs/connection";
import { QueryTypes } from 'sequelize';



export default class RenovacionesService {
  static async getSolicitudesPago(id: string, query: string){
    return await dbPostgres.query(`
    SELECT
\tDISTINCT ON (operaciones."prepagoReno"."idEmisiones") "idEmisiones" as ID,
    generales.emisiones."idEmpleadoRenovacion",
    generales.emisiones.poliza,
    generales.emisiones."fechaRegistro",
\t"recursosHumanos".precandidato.nombre || ' ' ||
    "recursosHumanos".precandidato."apellidoPaterno" || ' ' ||
    "recursosHumanos".precandidato."apellidoMaterno" AS "nombreEmpleado",
    operaciones.cliente.nombre || ' ' ||
    operaciones.cliente.paterno || ' ' ||
    operaciones.cliente.materno AS "nombreCliente",
    operaciones."tipoSubRamo".tipo as descripcion,
    operaciones."prepagoReno".titular,
    operaciones."prepagoReno"."fechaPago",
    operaciones."prepagoReno"."numeroTarjeta",
    operaciones."prepagoReno".csv,
    operaciones."prepagoReno"."mesAnioVencimiento" as "vencimiento",
    operaciones."msiAhorraPay".nombre as "tipoPago",
    operaciones."metodoPagoAhorraPay".nombre as "metodoPago",
    operaciones."bancoAhorraPay".nombre as "banco",
    operaciones.socios."alias" as "nombreComercial",
    operaciones."prepagoReno"."tarjetaDigital"
FROM operaciones."prepagoReno"
    INNER JOIN generales.emisiones ON (operaciones."prepagoReno"."idEmisiones" = generales.emisiones.id)
    INNER JOIN operaciones."msiAhorraPay" ON operaciones."prepagoReno"."idMsiAhorraPay" = operaciones."msiAhorraPay".id
    INNER JOIN operaciones."bancoAhorraPay" ON operaciones."msiAhorraPay"."idBancoAhorraPay" = operaciones."bancoAhorraPay".id
    INNER JOIN operaciones."metodoPagoAhorraPay" ON operaciones."bancoAhorraPay"."idMetodoPagoAhorraPay" = operaciones."metodoPagoAhorraPay".id
    INNER JOIN operaciones.socios ON operaciones."metodoPagoAhorraPay"."idSocio" = operaciones.socios.id
    INNER JOIN "recursosHumanos".empleado ON generales.emisiones."idEmpleadoRenovacion" = "recursosHumanos".empleado.id
    INNER JOIN "recursosHumanos".candidato ON "recursosHumanos".empleado."idCandidato" = "recursosHumanos".candidato.id
    INNER JOIN "recursosHumanos".precandidato ON "recursosHumanos".candidato."idPrecandidato" = "recursosHumanos".precandidato.id
    INNER JOIN operaciones.registro ON generales.emisiones."idRegistro" = operaciones.registro.id
    INNER JOIN operaciones."productoCliente" ON operaciones.registro."idProducto" = operaciones."productoCliente".id
    INNER JOIN operaciones.cliente ON operaciones."productoCliente"."idCliente" = operaciones.cliente.id
    INNER JOIN operaciones."subRamo" ON operaciones."subRamo".id = operaciones."productoCliente"."idSubRamo"
    INNER JOIN operaciones."tipoSubRamo" ON operaciones."tipoSubRamo".id = operaciones."subRamo"."idTipoSubRamo"
    LEFT JOIN operaciones."productoCliente" AS nuevo_producto ON nuevo_producto."idEmision" = generales.emisiones.id
    LEFT JOIN operaciones.registro AS nuevo_registro ON nuevo_producto.id = nuevo_registro."idProducto"
    LEFT JOIN operaciones.recibos AS nuevo_recibo ON nuevo_registro.id = nuevo_recibo."idRegistro"
    LEFT JOIN operaciones.pagos AS nuevo_pago ON nuevo_recibo.id = nuevo_pago."idRecibo"
    ${query}
    ORDER BY
    "idEmisiones", operaciones."prepagoReno".id DESC`,
    {
      type: QueryTypes.SELECT
    });
  }

  static async updateImagePrimaTotal(id: number, primaTotal: number){
    return await dbPostgres.query(`
      UPDATE 
        generales.emisiones 
      SET	
        imagen = 1,
        "primaTotal" = ${primaTotal}
      WHERE 
        id  = ${id};
    `, {
      type: QueryTypes.UPDATE,
      plain: true
    })
  }

  static async updateEstadoRenovacion(id: number){
    return await dbPostgres.query(`
      UPDATE 
        generales.emisiones 
      SET	
        "idEstadoRenovacion" = 2
      WHERE 
        id  = ${id};
    `, {
      type: QueryTypes.UPDATE,
      plain: true
    })
  }

  static async getIdUserByIdRegistro(idRegistro: string){
    return await dbPostgres.query(`
      SELECT  
        operaciones.cliente.nombre || ' ' || operaciones.cliente.paterno AS "nombre",
        operaciones.registro.poliza,
        operaciones.cliente."idUsuarioApp"
      FROM
        operaciones.registro
        INNER JOIN operaciones."productoCliente" ON operaciones.registro."idProducto" = operaciones."productoCliente".id
        INNER JOIN operaciones.cliente ON operaciones."productoCliente"."idCliente" = operaciones.cliente.id
      WHERE
        operaciones.registro.id = ${idRegistro}; 
    `,
    {
      type: QueryTypes.SELECT,
      plain: true
    })}

    static async getPermisosVisualizacion(idEmpleado: string): Promise<any> {
      return await dbPostgres.query(`
          SELECT gu."idPermisosVisualizacion" as "idPermisoVisualizacion",
          pl."idDepartamento",
          u.id AS "idUsuario",
          pl."idPuesto",
          pl.id
          FROM operaciones.usuarios u
          INNER JOIN generales."grupoUsuarios" gu ON gu.id = u."idGrupo"
          INNER JOIN "recursosHumanos".empleado e ON e.id = u."idEmpleado"
          INNER JOIN "recursosHumanos".plaza pl ON pl.id = e."idPlaza"
          WHERE e.id = ${idEmpleado}
      `, {
          type: QueryTypes.SELECT,
          plain: true,
      })}
      
  static async selectEmisionByRegistro(idRegistro: string){
    return await dbPostgres.query(`
      SELECT 
        id
      FROM
        generales.emisiones
      WHERE
        "idRegistro" = ${idRegistro}
    `,{
      type: QueryTypes.SELECT,
      plain: true
    })}
}

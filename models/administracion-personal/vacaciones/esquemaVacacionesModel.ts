import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const EsquemaVacacionesModel = dbPostgres.define('esquemaVacaciones', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    descripcion: {
        type: DataTypes.STRING,
    },
    json: {
        type: DataTypes.JSON,
    },
    activo: {
        type: DataTypes.SMALLINT
    }
}, {
    schema: "recursosHumanos",
    timestamps: false,
    tableName: "esquemaVacaciones",
});

export default EsquemaVacacionesModel;

// import {Sequelize, Model, DataTypes} from "sequelize";
// import db from "../../db/connection";
//
// const sequelize = db;
//
// class EstadoSolicitudVacaciones extends Model {
//     public id!: number; // Note that the `null assertion` `!` is required in strict mode.
//     public descripcion!: string;
//     public activo!: number;
// }
//
// EstadoSolicitudVacaciones.init(
//     {
//         id: {
//             type: DataTypes.INTEGER.UNSIGNED,
//             autoIncrement: true,
//             primaryKey: true,
//         },
//         descripcion: {
//             type: DataTypes.STRING,
//         },
//         activo: {
//             type: DataTypes.INTEGER,
//         },
//     },
//     {
//         tableName: "estadoSolicitudVacaciones",
//         timestamps: false,
//         sequelize, // passing the `sequelize` instance is required
//     }
// );
//
// export default EstadoSolicitudVacaciones;

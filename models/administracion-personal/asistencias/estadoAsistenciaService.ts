import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const EstadoAsistenciaModel  = dbPostgres.define('estadoAsistenciaModel',
    {
        id: {
            type: DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true
        },
        descripcion: {
            type: DataTypes.STRING,
        },
        activo: {
            type: DataTypes.INTEGER,
        },
    },
    {
        tableName: "estadoAsistencia",
        timestamps: false,
        schema: "recursosHumanos"
    }
);

export default EstadoAsistenciaModel;

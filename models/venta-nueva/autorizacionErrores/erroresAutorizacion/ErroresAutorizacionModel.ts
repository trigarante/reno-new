import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../../configs/connection";

const ErroresAutorizacionModel = dbPostgres.define('ErroresAutorizacionModel', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idAutorizacionRegistro: {
        type: DataTypes.INTEGER
    },
    idEmpleado: {
        type: DataTypes.INTEGER
    },
    idEstadoCorreccion: {
        type: DataTypes.INTEGER
    },
    idEmpleadoCorreccion: {
        type: DataTypes.INTEGER
    },
    idTipoDocumento: {
        type: DataTypes.INTEGER
    },
    fechaCreacion: {
        type: DataTypes.DATE
    },
    fechaCorreccion: {
        type: DataTypes.DATE
    },
    correcciones: {
        type: DataTypes.JSON
    },
}, {
    timestamps: false,
    schema: "operaciones",
    tableName: "erroresAutorizacion",
});

export default ErroresAutorizacionModel


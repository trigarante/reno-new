import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const RecibosModel = dbPostgres.define('recibos', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idRegistro: {
        type: DataTypes.INTEGER,
    },
    idEmpleado: {
        type: DataTypes.INTEGER
    },
    idEstadoRecibos: {
        type: DataTypes.INTEGER
    },
    idEmpleadoAplicado: {
        type: DataTypes.INTEGER
    },
    idClavesSocios: {
        type: DataTypes.INTEGER
    },
    numero: {
        type: DataTypes.SMALLINT
    },
    cantidad: {
        type: DataTypes.DOUBLE
    },
    fechaVigencia: {
        type: DataTypes.DATE
    },
    fechaLiquidacion: {
        type: DataTypes.DATE
    },
    fechaPromesaPago: {
        type: DataTypes.DATE
    },
    fechaCierre: {
        type: DataTypes.DATE
    },
    comprobante: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.SMALLINT
    },
    fechaAplicacion: {
        type: DataTypes.DATE
    },

}, {
    tableName: "recibos",
    schema: "operaciones",
    timestamps: false
});
export default RecibosModel;

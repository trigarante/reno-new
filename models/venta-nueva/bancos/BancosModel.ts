import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";


const BancosModel = dbPostgres.define('bancos', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    nombre: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.INTEGER
    },
}, {
    tableName: "bancos",
    schema: "operaciones",
    timestamps: false
})

export default BancosModel

import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../../configs/connection";

const DetalleInternoModel = dbPostgres.define('detalleInterno', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idMarca: {
        type: DataTypes.INTEGER,
    },
    marca: {
        type: DataTypes.STRING
    },
    idModelo: {
        type: DataTypes.INTEGER
    },
    modelo: {
        type: DataTypes.STRING
    },
    idDescripcion: {
        type: DataTypes.INTEGER
    },

    descripcion: {
        type: DataTypes.STRING
    },
    idSubDescripcion: {
        type: DataTypes.INTEGER
    },
    subDescripcion: {
        type: DataTypes.STRING
    },
    detalle: {
        type: DataTypes.STRING
    },

}, {
    timestamps: false,
    schema: "operaciones",
    tableName: "detalleInterno",
});

export default DetalleInternoModel;

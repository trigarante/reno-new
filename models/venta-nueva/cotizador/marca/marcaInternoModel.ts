import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../../configs/connection";

const MarcaInternoModel = dbPostgres.define('marcaInterno', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    nombre: {
        type: DataTypes.STRING
    },
    idTipoSubRamo: {
        type: DataTypes.INTEGER
    },

}, {
    timestamps: false,
    schema: "operaciones",
    tableName: "marcaInterno",
});

export default MarcaInternoModel;

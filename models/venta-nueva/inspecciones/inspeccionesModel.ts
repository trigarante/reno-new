import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const InspeccionesModel = dbPostgres.define('inspecciones', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idRegistro: {
        type: DataTypes.BIGINT,
    },
    idEstadoInspeccion: {
        type: DataTypes.BIGINT
    },
    cp: {
        type: DataTypes.INTEGER
    },
    idColonia: {
        type: DataTypes.INTEGER
    },
    calle: {
        type: DataTypes.STRING
    },
    numInt: {
        type: DataTypes.STRING
    },
    numExt: {
        type: DataTypes.STRING
    },
    fechaInspeccion: {
        type: DataTypes.DATE
    },
    kilometraje: {
        type: DataTypes.BIGINT
    },
    placas: {
        type: DataTypes.STRING
    },
    urlPreventa: {
        type: DataTypes.STRING
    },
    urlPostventa: {
        type: DataTypes.STRING
    },
    archivo: {
        type: DataTypes.STRING
    },
    comentarios: {
        type: DataTypes.STRING
    },
}, {
    tableName: "inspecciones",
    schema: "operaciones",
    timestamps: false
});

export default InspeccionesModel;

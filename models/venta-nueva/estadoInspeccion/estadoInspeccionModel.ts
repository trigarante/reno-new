import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";


const EstadoInspeccionModel = dbPostgres.define('estadoInspeccion', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    estado: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.INTEGER
    },
}, {
    tableName: "estadoInspeccion",
    schema: "operaciones",
    timestamps: false
})

export default EstadoInspeccionModel

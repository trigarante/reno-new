import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";


const CarrierModel = dbPostgres.define('carrier', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    carrier: {
        type: DataTypes.STRING
    },
    activo: {
        type: DataTypes.INTEGER
    },
}, {
    tableName: "carrierTarjetas",
    schema: "operaciones",
    timestamps: false
})

export default CarrierModel

import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../../../configs/connection";

const ClienteModel = dbPostgres.define('cliente', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idPais: {
        type: DataTypes.INTEGER
    },
    razonSocial: {
        type: DataTypes.INTEGER
    },
    nombre: {
        type: DataTypes.STRING
    },
    paterno: {
        type: DataTypes.STRING
    },
    materno: {
        type: DataTypes.STRING
    },
    cp: {
        type: DataTypes.STRING
    },
    calle: {
        type: DataTypes.STRING
    },
    numInt: {
        type: DataTypes.STRING
    },
    numExt: {
        type: DataTypes.STRING
    },
    idColonia: {
        type: DataTypes.INTEGER
    },
    genero: {
        type: DataTypes.STRING
    },
    telefonoFijo: {
        type: DataTypes.STRING
    },
    telefonoMovil: {
        type: DataTypes.STRING
    },
    correo: {
        type: DataTypes.STRING
    },
    // fechaNacimiento: {
    //     type: DataTypes.DATE
    // },
    curp: {
        type: DataTypes.STRING
    },
    rfc: {
        type: DataTypes.STRING
    },
    archivo: {
        type: DataTypes.STRING
    },
    archivoSubido: {
        type: DataTypes.INTEGER
    },
    ruc: {
        type: DataTypes.STRING
    },
    dni: {
        type: DataTypes.STRING
    },
    idColoniaPeru: {
        type: DataTypes.INTEGER
    },
}, {
    timestamps: false,
    schema: "operaciones",
    tableName: "cliente",
});

export default ClienteModel;

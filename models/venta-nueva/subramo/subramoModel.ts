import {dbPostgres} from "../../../configs/connection";
import {DataTypes} from "sequelize";

const SubRamoModel = dbPostgres.define('subramo', {
    id: {
        type: DataTypes.INTEGER,
        autoIncrement: true,
        primaryKey: true,
    },
    idRamo: {
        type: DataTypes.INTEGER,
    },
    idTipoSubRamo: {
        type: DataTypes.INTEGER
    },
    descripcion: {
        type: DataTypes.STRING
    },
    prioridad: {
        type: DataTypes.SMALLINT
    },
    activo: {
        type: DataTypes.SMALLINT
    },
}, {
    tableName: "subRamo",
    schema: 'operaciones',
    timestamps: false
});

export default SubRamoModel;

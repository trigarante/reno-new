import {DataTypes} from "sequelize";
import {dbPostgres} from "../../../configs/connection";

const TemaTrigaranteEscucha = dbPostgres.define('temaTrigaranteEscuchaModel',
    {
        id: {
            type: DataTypes.INTEGER.UNSIGNED,
            autoIncrement: true,
            primaryKey: true,
        },

        descripcion: {
            type: DataTypes.STRING,
            validate: {
                notEmpty: true,
                isString(value) {
                    if (typeof value !== 'string') { throw new Error('commentary only String is allowed')}
                }
            }
        },
        activo: {
            type: DataTypes.INTEGER,
            validate: {
                isInt: true,
            }
        }
    },
    {
        tableName: "temaTrigaranteEscucha",
        timestamps: false,
        schema: "recursosHumanos"
    }
);

export default TemaTrigaranteEscucha;

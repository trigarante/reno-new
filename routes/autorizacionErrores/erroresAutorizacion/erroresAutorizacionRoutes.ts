import {Request, Response, Router} from "express";
import ErroresAutorizacionQueries
    from "../../../queries/AutorizacionErrores/ErroresAutorizacion/ErroresAutorizacionQueries";
import AutorizacionRegistroQueries
    from "../../../queries/AutorizacionErrores/autorizacionRegistro/autorizacionRegistroQueries";
import ErroresAnexosAQueries from "../../../queries/erroresAnexos/erroresAnexosAQueries";
const generales = require('../../../general/function');
const erroresAutorizacionRoutes = Router();

erroresAutorizacionRoutes.get('/:idAutorizacionRegistro/:idTipoDocumento', async (req: Request, res: Response, next) => {
    try {
        const {idAutorizacionRegistro,idTipoDocumento} = req.params;
        if (!(idAutorizacionRegistro && idTipoDocumento)) return generales.manejoErrores('No se envio el id', res);
        const tiposDocumentos = await ErroresAutorizacionQueries.getByIdAutorizacionAndDocumento(idAutorizacionRegistro, idTipoDocumento);
        res.status(200).send(tiposDocumentos);
    } catch (err) {
        next(err);
    }
})

erroresAutorizacionRoutes.post('/', async (req: Request, res: Response, next) => {
    try {
        await ErroresAutorizacionQueries.post(req.body)
        return res.status(200).send();
    } catch (err) {
        next(err)
    }
});

erroresAutorizacionRoutes.put('/correccion-datos', async(req:Request, res: Response, next) => {
    try {
        const { idAutorizacionRegistro, documentoVerificado, idError} = req.body;
        if (!(idAutorizacionRegistro && documentoVerificado && idError)) return generales.manejoErrores('No se enviaron los parametros requeridos', res);
        let autorizacion = await AutorizacionRegistroQueries.getByIdEstado(idAutorizacionRegistro)
        switch (documentoVerificado) {
            case 1:
                autorizacion.verificadoCliente = 3
                break;
            case 2:
                autorizacion.verificadoRegistro = 3
                break;
            case 3:
                autorizacion.verificadoProducto = 3
                break;
            case 4:
                autorizacion.verificadoPago = 3
                break
        }
        await AutorizacionRegistroQueries.updateEstado(idAutorizacionRegistro, autorizacion);
        await ErroresAutorizacionQueries.update({idEstadoCorreccion: 2, fechaCorreccion: new Date()}, idError);
        res.status(200).send();
    } catch (e) {
        res.sendStatus(500);
    }
});

erroresAutorizacionRoutes.put('/:idErrorAutorizacion', async(req:Request, res: Response, next) => {
    try {
        const {idErrorAutorizacion} = req.params;
        if (!idErrorAutorizacion) return generales.manejoErrores('No se envio el id', res);
        await ErroresAutorizacionQueries.update(req.body, idErrorAutorizacion)
        res.status(200).send()
    } catch (e) {
        res.sendStatus(500);
    }
});

export default erroresAutorizacionRoutes;

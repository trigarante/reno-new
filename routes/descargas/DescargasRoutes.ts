import {Request, Response, Router} from "express";
import DescargasQueries from "../../queries/descargas/descargasQueries";

const DescargasRoutes = Router();

// Dashboard's routes
DescargasRoutes.get('/avance-ejecutivos/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {
        const {fechaInicio, fechaFin} = req.params;
        const polizas = await DescargasQueries.avanceEjecutivos(fechaInicio, fechaFin);
        res.status(200).send(polizas);
    } catch (err) {
        next(err);
    }
});
DescargasRoutes.get('/llamadas/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {
        const {fechaInicio, fechaFin} = req.params;
        const polizas = await DescargasQueries.llamadas(fechaInicio, fechaFin);
        res.status(200).send(polizas);
    } catch (err) {
        next(err);
    }
});
DescargasRoutes.get('/avance/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {
        const {fechaInicio, fechaFin} = req.params;
        const polizas = await DescargasQueries.avance(fechaInicio, fechaFin);
        res.status(200).send(polizas);
    } catch (err) {
        next(err);
    }
});
DescargasRoutes.get('/pnc/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {
        const {fechaInicio, fechaFin} = req.params;
        const polizas = await DescargasQueries.pnc(fechaInicio, fechaFin);
        res.status(200).send(polizas);
    } catch (err) {
        next(err);
    }
});
DescargasRoutes.get('/pncSUB/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {
        const {fechaInicio, fechaFin} = req.params;
        const polizas = await DescargasQueries.pncSUB(fechaInicio, fechaFin);
        res.status(200).send(polizas);
    } catch (err) {
        next(err);
    }
});
DescargasRoutes.get('/llamadas-reno-entrada/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {
        const {fechaInicio, fechaFin} = req.params;
        const polizas = await DescargasQueries.acd(fechaInicio, fechaFin);
        res.status(200).send(polizas);
    } catch (err) {
        next(err);
    }
});
DescargasRoutes.get('/llamadas-reno-salida/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {
        const {fechaInicio, fechaFin} = req.params;
        const polizas = await DescargasQueries.out(fechaInicio, fechaFin);
        res.status(200).send(polizas);
    } catch (err) {
        next(err);
    }
});

export default DescargasRoutes;

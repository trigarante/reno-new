import {Request, Response, Router} from "express";
import generales from "../../general/function";
import ProductoSolicitudQueries from "../../queries/prospecto/ProductoSolicitudQueries";
import CotizacionesQueries from "../../queries/cotizador/cotizacionesQueries";
import CotizacionesAliQueries from "../../queries/cotizador/cotizacionesAliQueries";
import SolicitudesInternoQueries from "../../queries/solicitudesInterno/solicitudesInternoQueries";

const router = Router();

router.get('/getById', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se envio el id', res);
        const producto = await ProductoSolicitudQueries.getById(id);
        res.status(200).send(producto);
    } catch (e) {
        res.status(500).send(e)
    }
})

router.post('/All', async(req:Request, res: Response, next) => {
    try {
        const {idempleado, iddepartamento} = req.headers;
        if (!(idempleado && iddepartamento)) return generales.manejoErrores('No se envio ningun id', res);
        const idProducto = await ProductoSolicitudQueries.post(req.body);
        const cotizaciones = {
            idProducto,
            idTipoContacto: 3,
        };
        const idCotizacion = await CotizacionesQueries.post(cotizaciones);
        const cotizacionesAli = {
            idCotizacion,
            idSubRamo: 50,
            peticion: req.body.datos,
        };
        const idCotizacionAli = await CotizacionesAliQueries.post(cotizacionesAli);
        const solicitud = {
            idCotizacionAli,
            idEmpleado: +idempleado,
            idDepartamento: +iddepartamento,
            idEstadoSolicitud: 2,
        };
        const idSolicitud = await SolicitudesInternoQueries.post(solicitud);
        res.status(200).send({idSolicitud});
    } catch (e) {
        res.status(500).send(e)
    }
})

export default router;

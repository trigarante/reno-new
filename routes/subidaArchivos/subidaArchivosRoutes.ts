import {Request, Response, Router} from "express";
const generales = require('../../general/function');

import DriveApiService from "../../services/drive/driveApiService";
import RegistroQueries from "../../queries/registro/RegistroQueries";
import ProductoClienteQueries from "../../queries/solicitudes/producto-cliente/producto-cliente-queries";
import PagosQueries from "../../queries/pagos/pagosQueries";
import routes from "../index";
import AutorizacionRegistroQueries
    from "../../queries/AutorizacionErrores/autorizacionRegistro/autorizacionRegistroQueries";
import ErroresAnexosAQueries from "../../queries/erroresAnexos/erroresAnexosAQueries";
import VerificacionRegistroQueries from "../../queries/verificacionRegistro/verificacionRegistroQueries";
import ErroresAnexosVQueries from "../../queries/erroresAnexosVerificacion/erroresAnexosVQueries";

const SubidaArchivosRoutes = Router();

// Subida de archivos a drive producto cliente
SubidaArchivosRoutes.post('/archivos-faltantes', async (req: Request, res: Response, next) => {
        if (!(req['files'] && req['files'].file)) return generales.manejoErrores('No se envio ningún archivo', res);
        // Recibo archivo
        const files = req['files'].file;
        const {id, numero} = req.body;
        if (!(id && numero)) return generales.manejoErrores('No se envio la información', res);
        console.log(id);
        const registro = await RegistroQueries.getAllIdsSteps(id);
        let nombre;
        switch (+numero) {
            case 1:
                nombre = registro.idCliente + '-cliente';
                break;
            case 2:
                nombre = registro.id + '-poliza';
                break;
            case 3:
                nombre = registro.id + '-pago-1';
                break;
            default:
                nombre = registro.id + '-inspeccion';
                break;
        }
        const idFolder = await DriveApiService.findOrCreateFolder(1, registro.idCliente);
        console.log(idFolder);
        let archivo;
        if (+numero !== 1) {
            const idFolderPago = await DriveApiService.findOrCreateFolder(2, registro.id, idFolder);
            const aux = await DriveApiService.subirArchivo(idFolderPago, files, nombre)
            archivo = +numero === 4 ? aux : idFolderPago;
        } else {
            await DriveApiService.subirArchivo(idFolder, files, nombre)
            archivo = idFolder;
        }
        // ACtualizar con otro case lo que se necesita
        switch (+numero) {
            case 1:
                await ProductoClienteQueries.update({archivo}, registro.idProducto);
                break;
            case 2:
                await RegistroQueries.update({archivo}, registro.id);
                break;
            case 3:
                await PagosQueries.update({archivo}, registro.idPago);
                break;
            default:
                // await Inspecc.update({archivo}, registro.idPago);
                break;
        }
        return res.status(200).send({id});
});

SubidaArchivosRoutes.post('/correccion-documentos', async (req: Request, res: Response, next) => {
    try {
        if (!(req['files'] && req['files'].file)) return generales.manejoErrores('No se envio ningún archivo', res);
        const files = req['files'].file;
        const {id, numero, idAutorizacionRegistro, idError, esVerificacion} = req.body;
        if (!(id && numero && idAutorizacionRegistro && idError ))
            return generales.manejoErrores('No se envio la información', res);
        console.log(id);
        const registro = await RegistroQueries.getAllIdsSteps(+id);
        let nombre;
        switch (+numero) {
            case 1:
                nombre = registro.idCliente + '-cliente';
                break;
            case 2:
            case 3:
                nombre = registro.id + '-poliza';
                break;
            case 4:
                nombre = registro.id + '-pago-1';
                break;
            default:
                nombre = registro.id + '-inspeccion';
                break;
        }
        const idFolder = await DriveApiService.findOrCreateFolder(1, registro.idCliente);
        console.log(idFolder);
        let archivo;
        if (+numero !== 1) {
            const idFolderPago = await DriveApiService.findOrCreateFolder(2, registro.id, idFolder);
            const aux = await DriveApiService.subirArchivo(idFolderPago, files, nombre)
            archivo = +numero === 4 ? aux : idFolderPago;
        } else {
            await DriveApiService.subirArchivo(idFolder, files, nombre)
            archivo = idFolder;
        }
        switch (+numero) {
            case 1:
                await ProductoClienteQueries.update({archivo}, registro.idProducto);
                break;
            case 2:
            case 3:
                await RegistroQueries.update({archivo}, registro.id);
                break;
            case 4:
                await PagosQueries.update({archivo}, registro.idPago);
                break;
            default:
                // await Inspecc.update({archivo}, registro.idPago);
                break;
        }

        // Fin subida archivo
        let autorizacion = await !+esVerificacion ? AutorizacionRegistroQueries.getByIdEstado(idAutorizacionRegistro) :
            VerificacionRegistroQueries.getByIdEstado(idAutorizacionRegistro)
        switch (+numero) {
            case 1:
                autorizacion.verificadoCliente = 3
                break;
            case 2:
                autorizacion.verificadoRegistro = 3
                break;
            case 3:
                autorizacion.verificadoProducto = 3
                break;
            case 4:
                autorizacion.verificadoPago = 3
                break
        }

        await !+esVerificacion ? AutorizacionRegistroQueries.updateEstado(idAutorizacionRegistro, autorizacion) :
            VerificacionRegistroQueries.updateEstado(idAutorizacionRegistro, autorizacion);

        // Fin actualizacion Autorizacion
        await !+esVerificacion ? ErroresAnexosAQueries.update({idEstadoCorreccion: 2, fechaCorreccion: new Date()}, +idError)
            : ErroresAnexosVQueries.update({idEstadoCorreccion: 2, fechaCorreccion: new Date()}, +idError)

        // Fin Actualizacion error
        return res.status(200).send();
    } catch (err) {
        next(err)
    }
});

SubidaArchivosRoutes.get('/archivo-bajar', async (req: Request, res: Response, next) => {
        console.log(req.headers)
        const {archivoid, tipo} = req.headers;
        if (!(archivoid && tipo)) return generales.manejoErrores('No se envió el id de la carpeta', res);
        let file;
        let folder;
        switch (+tipo) {
            case 1:
                try {
                    folder = await DriveApiService.getIdOfMostRecentFile(archivoid, 'cliente');
                    file = await DriveApiService.obtenerArchivo(folder);
                }
                catch (err) {
                    file = await DriveApiService.obtenerArchivo(archivoid);
                }
                break;
            case 2:
            case 3:
                folder = await DriveApiService.getIdOfMostRecentFile(archivoid, 'poliza');
                file = await DriveApiService.obtenerArchivo(folder);
                break;
            case 4:
            case 5:
                file = await DriveApiService.obtenerArchivo(archivoid);
                break;
        }
        return res.status(200).send([file])
});
SubidaArchivosRoutes.put('/corregir-archivos', async (req: Request, res: Response, next) => {
    try {
        if (!(req['files'] && req['files'].file)) return generales.manejoErrores('No se envio ningún archivo', res);
        // Recibo archivo
        const files = req['files'].file;
        const {id, numero} = req.body;
        console.log(id, numero)
        if (!(id && numero)) return generales.manejoErrores('No se envio la información', res);
        const registro = await RegistroQueries.getAllIdsSteps(id);
        let nombre;
        switch (+numero) {
            case 1:
                nombre = registro.idCliente + '-cliente';
                break;
            case 2:
            case 3:
                nombre = registro.id + '-poliza';
                break;
            case 4:
                nombre = registro.id + '-pago-1';
                break;
            default:
                nombre = registro.id + '-inspeccion';
                break;
        }
        const idFolder = await DriveApiService.findOrCreateFolder(1, registro.idCliente);
        let archivo;
        if (+numero !== 1) {
            const idFolderPago = await DriveApiService.findOrCreateFolder(2, registro.id, idFolder);
            const aux = await DriveApiService.subirArchivo(idFolderPago, files, nombre)
            archivo = +numero === 2 ? idFolderPago : aux;
        } else {
            await DriveApiService.subirArchivo(idFolder, files, nombre)
            archivo = idFolder;
        }
        // ACtualizar con otro case lo que se necesita
        switch (+numero) {
            case 1:
                await ProductoClienteQueries.update({archivo}, registro.idProducto);
                break;
            case 2:
            case 3:
                await RegistroQueries.update({archivo}, registro.id);
                break;
            case 4:
                await PagosQueries.update({archivo}, registro.idPago);
                break;
        }
        return res.status(200).send({id});
    } catch (err) {
        next(err)
    }
});

export default SubidaArchivosRoutes;

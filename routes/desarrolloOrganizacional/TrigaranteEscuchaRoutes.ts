import {Request, Response, Router} from 'express';
import globalVariables from "../../general/globalVariables";
import EmailExample from "../emailExample";
import EmpleadosQueries from "../../queries/empleados/empleadosQueries";
import TrigaranteEscuchaQueries from "../../queries/desarrolloOrganizacional/TrigaranteEscuchaQueries";
const uniqid = require('uniqid');
const generales = require('../../general/function')

const TrigaranteEscuchaRoutes = Router();


TrigaranteEscuchaRoutes.get('/getByActivos', async (req: Request, res: Response, next) => {
    try {
        const activo: number = +req.headers.activo;
        if (activo == null) return generales.manejoErrores('No se envió el id', res);
        const escucha = await TrigaranteEscuchaQueries.getActivos(activo);
        return res.status(200).send(escucha);
    } catch (err) {
        next(err);
    }
});
TrigaranteEscuchaRoutes.get('/getByTema', async (req: Request, res: Response, next) => {
    try {
        const idTema: number = +req.headers.idtema;
        if (!idTema) return generales.manejoErrores('No se envió el id', res);
        const escucha = await TrigaranteEscuchaQueries.getByIdTema(idTema);
        return res.status(200).send(escucha);
    } catch (err) {
        next(err);
    }
});

TrigaranteEscuchaRoutes.post('', async (req: Request, res: Response, next) => {
    try {
        const folio = uniqid();
        const {idEmpleado} = req.body;
        const correo: any = await EmpleadosQueries.getCorreoPersonal(idEmpleado)
        if(!(correo && idEmpleado)) return generales.manejoErrores('No se envió la informacion', res);
        await TrigaranteEscuchaQueries.post({...req.body, folio})
        await EmailExample.emailSend(correo.email, globalVariables.escucha.title, globalVariables.escucha.mensaje, res)
        // await EmailExample.emailSend('trigarantescucha@trigarante.com', globalVariables.escucha.title, globalVariables.escucha.mensaje, res)
        // await EmailExample.emailSend('xluna@trigarante.com', globalVariables.escucha.title, globalVariables.escucha.mensaje, res)
        return res.status(200).send({folio});
    } catch (err) {
        next(err);
    }
});

export default TrigaranteEscuchaRoutes;

import {Router, Request, Response} from "express";
import AdministradorPolizasBandejasQueries from "../../queries/adminPolizas/administradorPolizasBandejas-queries";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
const generales = require('../../general/function');

const adminPolizasBandejasRoutes = Router();

adminPolizasBandejasRoutes.get('/:idTipoBandeja/:idEmpleado/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idTipoBandeja, idEmpleado, fechaInicio, fechaFin} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasBandejasQueries.getPermisosVisualizacion(+idEmpleado);
        let query = '';
        switch (permisos.idPermisoVisualizacion) {
            case 2:
                const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario);
                const aux = [];
                for (const departamento of departamentos) {
                    aux.push(departamento.idDepartamento);
                }
                if(aux.length === 0) {
                    aux.push(permisos.idDepartamento);
                }
                query = `and r."idDepartamento" IN (${aux})`;
                break;
            case 3:
                query = `and r."idEmpleado" = ${idEmpleado}`;
                break;
        }
        const adminPolizas = await AdministradorPolizasBandejasQueries.getAll(fechaInicio, fechaFin,idTipoBandeja, query)
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})

adminPolizasBandejasRoutes.get('/buscar-poliza/:poliza', async(req:Request, res: Response, next) => {
    try {
        const {poliza} = req.params;
        const {value} = req.headers;
        if (!(poliza && value)) return generales.manejoErrores('No se envio la información', res);
        const query = +value === 1 ? `and r."poliza" = '${poliza}'` : `and pc."noSerie" = '${poliza}'`;
        const adminPolizas = await AdministradorPolizasBandejasQueries.getAllByPoliza(query);
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})
export default adminPolizasBandejasRoutes;

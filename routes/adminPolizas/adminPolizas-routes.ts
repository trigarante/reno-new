import {Router, Request, Response} from "express";
import AdministradorPolizasQueries from "../../queries/adminPolizas/administradorPolizas-queries";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
const generales = require('../../general/function');

const adminPolizasRoutes = Router();

adminPolizasRoutes.get('/:idEmpleado/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idEmpleado, fechaInicio, fechaFin} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empleado', res);
        const permisos = await AdministradorPolizasQueries.getPermisosVisualizacion(+idEmpleado);
        let query = '';
        switch (permisos.idPermisoVisualizacion) {
            case 2:
                const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario)
                const aux = [];
                for (const departamento of departamentos) {
                    aux.push(departamento.idDepartamento)
                }
                if (aux.length === 0) {
                    aux.push(permisos.idDepartamento);
                }
                query = `and r."idDepartamento" IN (${aux})`
                break;
            case 3:
                query = `and r."idEmpleado" = ${idEmpleado}`;
                break;
        }
        const adminPolizas = await AdministradorPolizasQueries.getAll(fechaInicio, fechaFin, query)
        res.status(200).send( adminPolizas );
    } catch (e) {
        next(e)
    }
})


export default adminPolizasRoutes;

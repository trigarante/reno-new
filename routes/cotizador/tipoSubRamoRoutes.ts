import {Request, Response, Router} from "express";
import TipoSubRamoQueries from "../../queries/cotizador/tipoSubRamoQueries";

const router = Router();

router.get('/getFiltradas', async(req:Request, res: Response, next) => {
    try {
        const tipos = await TipoSubRamoQueries.getFiltrados();
        res.status(200).send(tipos);
    } catch (e) {
        res.status(500).send(e)
    }
})

export default router;

import {Router, Request, Response} from "express";
import RegistroQueries from "../../queries/registro/RegistroQueries";
import ProductoClienteQueries from "../../queries/solicitudes/producto-cliente/producto-cliente-queries";
import ReciboQueries from "../../queries/recibo/ReciboQueries";
import DriveApiService from "../../services/drive/driveApiService";
const generales = require('../../general/function');

const registroRoutes = Router();

registroRoutes.get('/viewer/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        console.log(id)
        const registroviewer = await RegistroQueries.findAllByIdRegistroViewer(id);
        res.status(200).send( registroviewer );
    } catch (e) {
        res.status(500).send(e)
    }
})

registroRoutes.get('/detalles-poliza/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        const registroviewer = await RegistroQueries.detallesPoliza(id);
        res.status(200).send( registroviewer );
    } catch (e) {
        res.status(500).send(e)
    }
})

registroRoutes.get('/existePoliza/poliza', async(req:Request, res: Response, next) => {
    try {
        const {poliza, idsocio, fechainicio} = req.headers;
        if (!(poliza && idsocio && fechainicio) ) return generales.manejoErrores('No se envio la información necesaria', res);
        const registroviewer = await RegistroQueries.getPolizaExiste(poliza, idsocio, fechainicio);
        res.status(200).send( registroviewer );
    } catch (e) {
        res.status(500).send(e)
    }
})

registroRoutes.post('/all-reno', async(req:Request, res: Response,  next) => {
    let polizaExist = null;
    try {
        const file = req['files'].file;
        let registros = JSON.parse(req.body.registro);
        console.log(req.body)
        let registro = registros.registro;
        const recibos = registros.recibos;
        const producto = await ProductoClienteQueries.getById(registro.idProducto);
        console.log(producto)
        registro.idProducto = producto.id;
        const idSocio = registro.idSocio;
        const poliza = registro.poliza;
        const fecha = registro.fechaInicio;
        polizaExist = await RegistroQueries.getPolizaRen(idSocio, poliza, fecha);
        const idRegistro = await RegistroQueries.post(registro);
        for (const recibo of recibos) {
            recibo.idRegistro = idRegistro;
            await ReciboQueries.post(recibo);
        }
        const idFolder = await DriveApiService.findOrCreateFolder(1, producto.idCliente);
        const idFolderPago = await DriveApiService.findOrCreateFolder(2, idRegistro, idFolder);
        const archivo = await DriveApiService.subirArchivo(idFolderPago, file, idRegistro + '-poliza')
        await RegistroQueries.update({archivo: idFolderPago}, idRegistro)
        res.status(200).send({idRegistro});
    } catch (e) {
        res.status(500).send(polizaExist)
    }
})

registroRoutes.get('/noSerieExiste', async (req: Request, res: Response, next) => {
    const {noserie} = req.headers;
    if(!noserie) return generales.manejoErrores('No se envio la informacion', res)
    const noSerie = await ProductoClienteQueries.findNoSerie(noserie);
    res.status(200).send(noSerie);
});

registroRoutes.get('/forInpecciones', async (req: Request, res: Response, next) => {
    const {id} = req.headers;
    if(!id) return generales.manejoErrores('No se envio la informacion', res)
    const data = await ProductoClienteQueries.getForInspecciones(id);
    res.status(200).send(data);
});
// Para actualizar el registro
registroRoutes.get('/getForRegistroById/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        const registro = await RegistroQueries.getForRegistroById(id);
        res.status(200).send( registro );
    } catch (e) {
        res.status(500).send(e)
    }
})

// NO SE USA
// Traer todos los id de los steps
// registroRoutes.get('/getAllIds', async(req:Request, res: Response, next) => {
//     try {
//         const {id} = req.headers;
//         if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
//         const registro = await RegistroQueries.getAllIdsSteps(id);
//         res.status(200).send( registro );
//     } catch (e) {
//         res.status(500).send(e)
//     }
// })

registroRoutes.get('/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        const registro = await RegistroQueries.findAllByIdRegistro(id);
        res.status(200).send( registro );
    } catch (e) {
        res.status(500).send(e)
    }
})

registroRoutes.put('/', async(req:Request, res: Response, next) => {
    try {
        const {id, ...data} = req.body;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        await RegistroQueries.update({...data}.registro, id);
        res.status(200).send();
    } catch (e) {
        res.status(500).send(e)
    }
})
export default registroRoutes;

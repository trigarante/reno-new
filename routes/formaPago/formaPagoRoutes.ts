import {Router, Request, Response} from "express";
import FormaPagoQueries from "../../queries/formaPago/formaPagoQueries";

const formaPagoRoutes = Router();

formaPagoRoutes.get('/', async(req:Request, res: Response, next) => {
    try {
        const formaPago = await FormaPagoQueries.findAll();
        res.status(200).send( formaPago );
    } catch (e) {
        res.status(500).send(e)
    }
})


export default formaPagoRoutes;

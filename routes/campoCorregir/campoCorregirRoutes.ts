import {Request, Response, Router} from "express";
import CampoCorregirQueries from "../../queries/campoCorregir/CampoCorregirQueries";
const generales = require('../../general/function');

const campoCorregirRoutes = Router();

campoCorregirRoutes.get('/forMoral/:idTabla', async (req: Request, res: Response, next) => {
    try {
        const {idTabla} = req.params;
        if (!idTabla) return generales.manejoErrores('No se envio el id tabla', res);
        const campoCorregir = await CampoCorregirQueries.getByIdTablaAndActivos(idTabla);
        res.status(200).send(campoCorregir);
    } catch (err) {
        next(err);
    }
})

campoCorregirRoutes.get('/:idTabla', async (req: Request, res: Response, next) => {
    try {
        const {idTabla} = req.params;
        if (!idTabla) return generales.manejoErrores('No se envio el id tabla', res);
        const campoCorregir = await CampoCorregirQueries.getByIdTablaAndActivos(idTabla);
        res.status(200).send(campoCorregir);
    } catch (err) {
        next(err);
    }
})

export default campoCorregirRoutes;

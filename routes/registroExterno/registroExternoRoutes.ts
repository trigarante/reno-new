import {Request, Response, Router} from "express";
import RegistroExternoQueries from "../../queries/registroExterno/registroExternoQueries";

const router = Router();

router.get('', async(req: Request, res: Response, next ) => {
    try {
        const datos = await RegistroExternoQueries.get();
        res.send(datos);
    } catch (e) {
        next(e)
    }
});

export default router;

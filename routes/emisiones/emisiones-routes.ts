import {Router, Request, Response} from "express";
import EmisionesQueries from "../../queries/emisiones/emisiones-queries";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
import PlazaQueries from "../../queries/RRHH/plazaQueries";
import router from "../socios/sociosRoutes";
const generales = require('../../general/function');

const emisionesRoutes = Router();

emisionesRoutes.get('/get/:idTipoBandeja/:idEmpleado/:fechaInicio/:fechaFin', async(req:Request, res: Response, next) => {
    try {
        const {idTipoBandeja, idEmpleado, fechaInicio, fechaFin} = req.params;
        if (!(idEmpleado && fechaInicio && fechaFin)) return generales.manejoErrores('No se envio el id empleado', res);
        let adminPolizas;
        adminPolizas = await EmisionesQueries.getAll(fechaInicio, fechaFin,idTipoBandeja)
        res.status(200).send( adminPolizas );
    } catch (e) {
        res.status(500).send(e)
    }
})
emisionesRoutes.get('/:id', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id) return generales.manejoErrores('No se envio ningun id', res);
        const socios = await EmisionesQueries.getAllById(id);
        res.status(200).send(socios);
    } catch (err) {
        next(err);
    }
})

emisionesRoutes.get('/pendientes/:fechaInicio/:fechaFin', async (req: Request, res: Response, next) => {
    try {
        const {fechaInicio, fechaFin} = req.params;
        const socios = await EmisionesQueries.getAll2(fechaInicio, fechaFin);
        res.status(200).send(socios);
    } catch (err) {
        next(err);
    }
})
export default emisionesRoutes;

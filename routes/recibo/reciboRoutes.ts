import {Router, Request, Response} from "express";
import ReciboQueries from "../../queries/recibo/ReciboQueries";
const generales = require('../../general/function');

const reciboRoutes = Router();

reciboRoutes.get('/registro/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        const recibo = await ReciboQueries.findAllByIdRegistroAndActivo(id);
        res.status(200).send( recibo );
    } catch (e) {
        res.status(500).send(e)
    }
})

reciboRoutes.get('/forPago/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Registro', res);
        const recibo = await ReciboQueries.forPagosByidRegistro(id);
        res.status(200).send( recibo );
    } catch (e) {
        res.status(500).send(e)
    }
})


export default reciboRoutes;

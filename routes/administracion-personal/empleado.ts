import { Request, Response, Router } from 'express'
import EmpleadoService from "../../queries/administracion-personal/empleados";
const generales = require('../../general/function');
const EmpleadoRoutes = Router();
EmpleadoRoutes.get('/getPlazasHijo', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se nvio el id', res);
        const { idPlaza } = await EmpleadoService.getById(id);
        const Empleados = await EmpleadoService.getPlazasHijo(+idPlaza);
        console.log(Empleados)
        return res.status(200).send(Empleados);
    } catch (err) {
        next(err);
    }
});
export default EmpleadoRoutes;

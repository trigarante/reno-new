import { Request, Response, Router } from 'express'
import MotivoJustificacionService from "../../queries/administracion-personal/motivoJustificacion";
const generales = require('../../general/function');
const MotivoJustificacionRoutes = Router();

MotivoJustificacionRoutes.get('', async (req: Request, res: Response, next) => {
    try {
        const MotivoJustificacions = await MotivoJustificacionService.getAll();
        return res.status(200).send(MotivoJustificacions);
    } catch (err) {
        next(err);
    }
});

MotivoJustificacionRoutes.get('/byidEstadoAsistencia', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se envio el id', res);
        const MotivoJustificacions = await MotivoJustificacionService.getByEstadoAsistencia(+id);
        return res.status(200).send(MotivoJustificacions);
    } catch (err) {
        next(err);
    }
});

MotivoJustificacionRoutes.get('/:id', async (req: Request, res: Response, next) => {
    try {
        const id = +(req.params.id);
        const MotivoJustificacions = await MotivoJustificacionService.getMotivoJustificacionById(id);
        return res.status(200).send(MotivoJustificacions);
    } catch (err) {
        next(err);
    }
});

MotivoJustificacionRoutes.post('', async (req: Request, res: Response, next) => {
    try {
        const MotivoJustificacions = await MotivoJustificacionService.create(req.body);
        return res.status(200).send(MotivoJustificacions);
    } catch (err) {
        next(err);
    }
});

MotivoJustificacionRoutes.put('/:id', async (req: Request, res: Response, next) => {
    try {
        const id = Number(req.params.id);
        const MotivoJustificacions = await MotivoJustificacionService.updateMotivoJustificacion(id, req.body);
        return res.status(200).send(MotivoJustificacions);
    } catch (err) {
        next(err);
    }
});

export default MotivoJustificacionRoutes;

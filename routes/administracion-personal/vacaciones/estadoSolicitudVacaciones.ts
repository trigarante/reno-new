import { Request, Response, Router } from 'express'
import EstadoSolicitudVacacionesService from '../../../queries/administracion-personal/vacaciones/estadoSolicitudVacaciones';

const EstadoSolicitudVacacionesRoutes = Router();


EstadoSolicitudVacacionesRoutes.get('', async (req: Request, res: Response, next) => {
    try {
        const EstadoSolicitudVacacioness = await EstadoSolicitudVacacionesService.getAll();
        return res.status(200).send(EstadoSolicitudVacacioness);
    } catch (err) {
        next(err);
    }
});

EstadoSolicitudVacacionesRoutes.get('/:id', async (req: Request, res: Response, next) => {
    try {
        const id = +(req.params.id);
        const EstadoSolicitudVacacioness = await EstadoSolicitudVacacionesService.getEstadoSolicitudVacacionesById(id);
        return res.status(200).send(EstadoSolicitudVacacioness);
    } catch (err) {
        next(err);
    }
});

EstadoSolicitudVacacionesRoutes.post('', async (req: Request, res: Response, next) => {
    try {
        const EstadoSolicitudVacacioness = await EstadoSolicitudVacacionesService.create(req.body);
        return res.status(200).send(EstadoSolicitudVacacioness);
    } catch (err) {
        next(err);
    }
});

EstadoSolicitudVacacionesRoutes.put('/:id', async (req: Request, res: Response, next) => {
    try {
        const id = Number(req.params.id);
        const EstadoSolicitudVacacioness = await EstadoSolicitudVacacionesService.updateEstadoSolicitudVacaciones(id, req.body);
        return res.status(200).send(EstadoSolicitudVacacioness);
    } catch (err) {
        next(err);
    }
});

export default EstadoSolicitudVacacionesRoutes;

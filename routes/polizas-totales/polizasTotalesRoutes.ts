import {Request, Response, Router} from 'express';
import Polizas from "../../queries/polizas-totales/polizasTotalesQueries";

const router = Router();

router.get('/polizas/:fechaInicio/:fechaFin',async(req:Request, res: Response, next) => {
        try {
            const {fechaInicio, fechaFin} = req.params;
            const polizasT = await Polizas.findAllPolizas(fechaInicio, fechaFin);
            return res.status(200).send(polizasT)
        } catch (e) {
            return res.status(500).json({
                ok: false,
                msg: 'error revisar consola del back'
            });
        }
});

router.get('/canceladas/:fechaInicio/:fechaFin',async(req:Request, res: Response, next) => {
    try {
        const {fechaInicio, fechaFin} = req.params;
        const polizasT = await Polizas.findAllCanceladas(fechaInicio, fechaFin);
        return res.status(200).send(polizasT)
    } catch (e) {
        return res.status(500).json({
            ok: false,
            msg: 'error revisar consola del back'
        });
    }
});

router.get('/poliza/:id',async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        const polizasT = await Polizas.findId(id);
        return res.status(200).send(polizasT)
    } catch (e) {
        return res.status(500).json({
            ok: false,
            msg: 'error revisar consola del back'
        });
    }
});

export default router;

import {Request, Response, Router} from "express";
import generales from "../../general/function";
import RamoQueries from "../../queries/ramo/ramoQueries";

const router = Router();

router.get('/getByIdSocio', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se envio ningun id', res);
        const ramos = await RamoQueries.getByIdSocio(id);
        res.status(200).send(ramos);
    } catch (err) {
        next(err);
    }
})

export default router;

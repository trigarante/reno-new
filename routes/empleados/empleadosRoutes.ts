import {Request, Response, Router} from "express";
import EmpleadosQueries from "../../queries/empleados/empleadosQueries";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
import SolicitudesInternoQueries from "../../queries/solicitudesInterno/solicitudesInternoQueries";
const generales = require('../../general/function');
const empleadosRoutes = Router();

empleadosRoutes.get('/:id', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id) return generales.manejoErrores('No se envio el id del Empleado', res);
        const empleado = await EmpleadosQueries.getById(id);
        res.status(200).send(empleado);
    } catch (err) {
        next(err);
    }
})

empleadosRoutes.get('/ejecutivos/activos', async (req: Request, res: Response, next) => {
    try {
        const {id} = req.headers;
        if (!id) return generales.manejoErrores('No se envio el id del Empleado', res);
        const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(id);
        const aux = [];
        for (const departamento of departamentos) {
            aux.push(departamento.idDepartamento)
        }
        const empleados = await EmpleadosQueries.getEmpleadosByIdDepartamento(aux)
        res.status(200).send(empleados);
    } catch (err) {
        next(err);
    }
})


export default empleadosRoutes;


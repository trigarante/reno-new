import {Router, Request, Response, NextFunction} from "express";
import ClienteQueries from "../../queries/cliente/cliente-queries";
const generales = require('../../general/function');

const clientePeruRoutes = Router();

clientePeruRoutes.get('/existeDni', async(req: Request, res: Response, next: NextFunction ) => {
    try {
        const {dni} = req.headers;
        const cliente: any = await ClienteQueries.dniCliente(dni);
        res.json(cliente);
    } catch (e) {
        res.status(500).json(e);
    }
});

clientePeruRoutes.post('', async(req:Request, res: Response, next) => {
    try {
        const id = await ClienteQueries.post(req.body);
        res.status(200).send({id});
    } catch (e) {
        res.sendStatus(500);
    }
});

clientePeruRoutes.put('/:idCliente', async(req:Request, res: Response, next) => {
    try {
        const {idCliente} = req.params;
        if (!idCliente) return generales.manejoErrores('No se envio el id', res);
        await ClienteQueries.update(req.body, idCliente);
        res.status(200).send()
    } catch (e) {
        res.sendStatus(500);
    }
});
clientePeruRoutes.get('/byId/:id', async(req:Request, res: Response, next) => {
    try {
        const {id} = req.params;
        if (!id ) return generales.manejoErrores('No se envio el id Cliente', res);
        const cliente = await ClienteQueries.clienteAll(id);
        res.status(200).send( cliente );
    } catch (e) {
        res.sendStatus(500);
    }
});
export default clientePeruRoutes;

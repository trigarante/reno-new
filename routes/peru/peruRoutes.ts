import {Request, Response, Router} from "express";
import generales from '../../general/function';
import CrearClienteQueries from "../../queries/solicitudes/crear-cliente/crear-cliente-queries";

const router = Router();

router.get('', async (req: Request, res: Response, next) => {
    try {
        const cp = req.headers.cp;
        if (!cp)
            return generales.manejoErrores('No se envió cp', res);
        const coloniaData = await CrearClienteQueries.getCapitalesByCp(cp);
        res.status(200).send( coloniaData );
    } catch (e) {
        res.status(500).send( e );
    }
});

export default router;

import { NextFunction, Request, Response, Router } from "express";
import UsuariosDepartamentoQueries from "../../queries/usuariosDepartamento/usuariosDepartamentoQueries";
import RenovacionesService from "../../services/renoService";

const RenovacionesRoutes = Router();

RenovacionesRoutes.get('/solicitudes-pago/:idEmpleado', async (req: Request, res: Response, next: NextFunction) => {
  try{
    const { idEmpleado } = req.params;
    const permisos = await RenovacionesService.getPermisosVisualizacion(idEmpleado);
    let query: string = '';
    switch (permisos.idPermisoVisualizacion) {
      case 2:
          const departamentos = await UsuariosDepartamentoQueries.getDepartamentoByIdUsuario(permisos.idUsuario);
          const aux: any[] = [];
          for (const departamento of departamentos) {
              aux.push(departamento.idDepartamento);
          }
          if(aux.length === 0) {
              aux.push(permisos.idDepartamento);
          }
          query = `AND operaciones.registro."idDepartamento" IN (${aux})`;
          break;
      case 3:
          query = `AND generales.emisiones."idEmpleadoRenovacion" = ${idEmpleado}`;
          break;
  }
    const emisiones = await RenovacionesService.getSolicitudesPago(idEmpleado, query);
    return res.status(200).send(emisiones);
  } catch (err) {
    next(err);
  }
});

RenovacionesRoutes.put('/update-emision', async (req: Request, res: Response, next: NextFunction) => {
  try{
    const { idEmision, primaTotal } = req.body;
    console.log('BODY', idEmision, primaTotal);
    const emisionUpdate = await RenovacionesService.updateImagePrimaTotal(idEmision, primaTotal);
    console.log(emisionUpdate);
    return res.status(200).send(emisionUpdate);
  } catch (err) {
    next(err);
  }
});

RenovacionesRoutes.put('/estatus-renovacion', async (req: Request, res: Response, next: NextFunction) => {
  try{
    const { idEmision} = req.body;
    console.log('BODY', idEmision);
    const emisionUpdate = await RenovacionesService.updateEstadoRenovacion(idEmision);
    return res.status(200).send(emisionUpdate);
  } catch (err) {
    next(err);
  }
});

RenovacionesRoutes.get('/info-notification/:idRegistro', async (req: Request, res: Response, next: NextFunction) => {
  try{
    const { idRegistro } = req.params;
    const infoNotification = await RenovacionesService.getIdUserByIdRegistro(idRegistro);
    console.log(infoNotification);
    return res.status(200).send(infoNotification);
  } catch (err) {
    next(err);
  }
});

RenovacionesRoutes.get('/emision-by-idregistro/:idRegistro', async (req: Request, resp: Response, next: NextFunction) => {
  try{
    const { idRegistro } = req.params;
    const idEmision = await RenovacionesService.selectEmisionByRegistro(idRegistro);
    return resp.status(200).send(idEmision);
  } catch (err) {
    next(err);
  }
});

export default RenovacionesRoutes;

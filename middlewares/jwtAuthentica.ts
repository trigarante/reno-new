import {Request, Response, Router} from "express";
const jwt = require('jsonwebtoken');

const rutaProtegida = Router();

rutaProtegida.use((req: Request, res: Response, next) => {
    const token = req.headers['access-token'];
    if (token) {
        jwt.verify(token, process.env.llave, (err, decoded) => {
            if (err) {
                return res.status(401).send({mensaje: '¿Quién eres y porqué quieres acceder? >:v'});
            } else {
                req.headers.idempleado = decoded.idEmpleado;
                next();
            }
        })
    } else {
        res.status(401).send({mensaje: 'No tienes permiso :C'});
    }
})

export default rutaProtegida;

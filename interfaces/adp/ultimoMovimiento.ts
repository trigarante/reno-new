export interface UltimoMovimiento {
    idEmpleado: number;
    idEmpleadoSupervisor: number;
    idMotivo?: number;
    documento: string | void;
    comentarios: string;
    fechaFin: string;
    fechaInicio: string;
    idEstadoAsistencia: number;
    idAsistencia: number;
    idCierre: number;
    idEmpleadoADP?: number;
    idEstadoJustificacion?: number;
    fechaRegistro?: string;
}
